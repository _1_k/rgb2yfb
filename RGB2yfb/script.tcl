############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project RGB2FB
set_top RGB2yfb
add_files RGB2FB/RGB2yfb/src/RGB2yfb.h
add_files RGB2FB/RGB2yfb/src/RGB2yfb.cpp
add_files -tb RGB2FB/RGB2yfb/src/testbench/dataset -cflags "-Wno-unknown-pragmas"
add_files -tb RGB2FB/RGB2yfb/src/testbench/RGB2yfb_tb.cpp -cflags "-Wno-unknown-pragmas"
add_files -tb RGB2FB/RGB2yfb/src/RGB2yfb.h -cflags "-Wno-unknown-pragmas"
open_solution "RGB2yfb"
set_part {xc7z010clg400-1} -tool vivado
create_clock -period 7.5 -name default
config_compile -no_signed_zeros=0 -unsafe_math_optimizations=0
config_export -format ip_catalog -rtl verilog -vivado_phys_opt place -vivado_report_level 0
config_sdx -optimization_level none -target none
config_schedule -effort medium -relax_ii_for_timing=0
config_bind -effort medium
set_clock_uncertainty 12.5%
#source "./RGB2FB/RGB2yfb/directives.tcl"
csim_design -clean
csynth_design
cosim_design -tool xsim
export_design -rtl verilog -format ip_catalog
