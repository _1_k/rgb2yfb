// ==============================================================
// File generated on Tue Feb 12 02:58:43 JST 2019
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XRGB2YFB_H
#define XRGB2YFB_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xrgb2yfb_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 Axilites_BaseAddress;
} XRgb2yfb_Config;
#endif

typedef struct {
    u32 Axilites_BaseAddress;
    u32 IsReady;
} XRgb2yfb;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XRgb2yfb_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XRgb2yfb_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XRgb2yfb_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XRgb2yfb_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XRgb2yfb_Initialize(XRgb2yfb *InstancePtr, u16 DeviceId);
XRgb2yfb_Config* XRgb2yfb_LookupConfig(u16 DeviceId);
int XRgb2yfb_CfgInitialize(XRgb2yfb *InstancePtr, XRgb2yfb_Config *ConfigPtr);
#else
int XRgb2yfb_Initialize(XRgb2yfb *InstancePtr, const char* InstanceName);
int XRgb2yfb_Release(XRgb2yfb *InstancePtr);
#endif

void XRgb2yfb_Start(XRgb2yfb *InstancePtr);
u32 XRgb2yfb_IsDone(XRgb2yfb *InstancePtr);
u32 XRgb2yfb_IsIdle(XRgb2yfb *InstancePtr);
u32 XRgb2yfb_IsReady(XRgb2yfb *InstancePtr);
void XRgb2yfb_EnableAutoRestart(XRgb2yfb *InstancePtr);
void XRgb2yfb_DisableAutoRestart(XRgb2yfb *InstancePtr);
u32 XRgb2yfb_Get_return(XRgb2yfb *InstancePtr);

void XRgb2yfb_Set_fb_out(XRgb2yfb *InstancePtr, u32 Data);
u32 XRgb2yfb_Get_fb_out(XRgb2yfb *InstancePtr);
void XRgb2yfb_Set_hsize(XRgb2yfb *InstancePtr, u32 Data);
u32 XRgb2yfb_Get_hsize(XRgb2yfb *InstancePtr);
void XRgb2yfb_Set_vsize(XRgb2yfb *InstancePtr, u32 Data);
u32 XRgb2yfb_Get_vsize(XRgb2yfb *InstancePtr);
void XRgb2yfb_Set_stride(XRgb2yfb *InstancePtr, u32 Data);
u32 XRgb2yfb_Get_stride(XRgb2yfb *InstancePtr);
void XRgb2yfb_Set_fsize(XRgb2yfb *InstancePtr, u32 Data);
u32 XRgb2yfb_Get_fsize(XRgb2yfb *InstancePtr);

void XRgb2yfb_InterruptGlobalEnable(XRgb2yfb *InstancePtr);
void XRgb2yfb_InterruptGlobalDisable(XRgb2yfb *InstancePtr);
void XRgb2yfb_InterruptEnable(XRgb2yfb *InstancePtr, u32 Mask);
void XRgb2yfb_InterruptDisable(XRgb2yfb *InstancePtr, u32 Mask);
void XRgb2yfb_InterruptClear(XRgb2yfb *InstancePtr, u32 Mask);
u32 XRgb2yfb_InterruptGetEnabled(XRgb2yfb *InstancePtr);
u32 XRgb2yfb_InterruptGetStatus(XRgb2yfb *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
