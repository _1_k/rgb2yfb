// ==============================================================
// File generated on Tue Feb 12 02:58:43 JST 2019
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xrgb2yfb.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XRgb2yfb_CfgInitialize(XRgb2yfb *InstancePtr, XRgb2yfb_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Axilites_BaseAddress = ConfigPtr->Axilites_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XRgb2yfb_Start(XRgb2yfb *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_AP_CTRL) & 0x80;
    XRgb2yfb_WriteReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_AP_CTRL, Data | 0x01);
}

u32 XRgb2yfb_IsDone(XRgb2yfb *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u32 XRgb2yfb_IsIdle(XRgb2yfb *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u32 XRgb2yfb_IsReady(XRgb2yfb *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XRgb2yfb_EnableAutoRestart(XRgb2yfb *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRgb2yfb_WriteReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_AP_CTRL, 0x80);
}

void XRgb2yfb_DisableAutoRestart(XRgb2yfb *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRgb2yfb_WriteReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_AP_CTRL, 0);
}

u32 XRgb2yfb_Get_return(XRgb2yfb *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_AP_RETURN);
    return Data;
}
void XRgb2yfb_Set_fb_out(XRgb2yfb *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRgb2yfb_WriteReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_FB_OUT_DATA, Data);
}

u32 XRgb2yfb_Get_fb_out(XRgb2yfb *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_FB_OUT_DATA);
    return Data;
}

void XRgb2yfb_Set_hsize(XRgb2yfb *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRgb2yfb_WriteReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_HSIZE_DATA, Data);
}

u32 XRgb2yfb_Get_hsize(XRgb2yfb *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_HSIZE_DATA);
    return Data;
}

void XRgb2yfb_Set_vsize(XRgb2yfb *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRgb2yfb_WriteReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_VSIZE_DATA, Data);
}

u32 XRgb2yfb_Get_vsize(XRgb2yfb *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_VSIZE_DATA);
    return Data;
}

void XRgb2yfb_Set_stride(XRgb2yfb *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRgb2yfb_WriteReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_STRIDE_DATA, Data);
}

u32 XRgb2yfb_Get_stride(XRgb2yfb *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_STRIDE_DATA);
    return Data;
}

void XRgb2yfb_Set_fsize(XRgb2yfb *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRgb2yfb_WriteReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_FSIZE_DATA, Data);
}

u32 XRgb2yfb_Get_fsize(XRgb2yfb *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_FSIZE_DATA);
    return Data;
}

void XRgb2yfb_InterruptGlobalEnable(XRgb2yfb *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRgb2yfb_WriteReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_GIE, 1);
}

void XRgb2yfb_InterruptGlobalDisable(XRgb2yfb *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRgb2yfb_WriteReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_GIE, 0);
}

void XRgb2yfb_InterruptEnable(XRgb2yfb *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_IER);
    XRgb2yfb_WriteReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_IER, Register | Mask);
}

void XRgb2yfb_InterruptDisable(XRgb2yfb *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_IER);
    XRgb2yfb_WriteReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_IER, Register & (~Mask));
}

void XRgb2yfb_InterruptClear(XRgb2yfb *InstancePtr, u32 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XRgb2yfb_WriteReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_ISR, Mask);
}

u32 XRgb2yfb_InterruptGetEnabled(XRgb2yfb *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_IER);
}

u32 XRgb2yfb_InterruptGetStatus(XRgb2yfb *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XRgb2yfb_ReadReg(InstancePtr->Axilites_BaseAddress, XRGB2YFB_AXILITES_ADDR_ISR);
}

