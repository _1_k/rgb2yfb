// ==============================================================
// File generated on Tue Feb 12 02:58:43 JST 2019
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xrgb2yfb.h"

extern XRgb2yfb_Config XRgb2yfb_ConfigTable[];

XRgb2yfb_Config *XRgb2yfb_LookupConfig(u16 DeviceId) {
	XRgb2yfb_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XRGB2YFB_NUM_INSTANCES; Index++) {
		if (XRgb2yfb_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XRgb2yfb_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XRgb2yfb_Initialize(XRgb2yfb *InstancePtr, u16 DeviceId) {
	XRgb2yfb_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XRgb2yfb_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XRgb2yfb_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

