set clock_constraint { \
    name clk \
    module RGB2yfb \
    port ap_clk \
    period 7.5 \
    uncertainty 0.938 \
}

set all_path {}

set false_path {}

