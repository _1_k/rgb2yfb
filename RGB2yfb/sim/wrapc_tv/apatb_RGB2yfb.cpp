// ==============================================================
// File generated on Mon Feb 11 21:16:06 +0900 2019
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================

#include <systemc>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <stdint.h>
#include "SysCFileHandler.h"
#define AP_INT_MAX_W 32678

#include "ap_int.h"

#include "hls_stream.h"

using namespace std;
using namespace sc_core;
using namespace sc_dt;
// [define_apint] ---------->
template< int D ,int U ,int TI ,int TD > struct ap_axiu ;
template<> struct  ap_axiu<24, 1, 1, 1> {
ap_uint<24> data;
ap_uint<3> keep;
ap_uint<3> strb;
ap_uint<1> user;
ap_uint<1> last;
ap_uint<1> id;
ap_uint<1> dest;
}; 



   #define AUTOTB_TVOUT_gmem  "../tv/cdatafile/c.RGB2yfb.autotvout_gmem.dat"
   #define AUTOTB_TVIN_video_in_V_data_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_data_V.dat"
   #define WRAPC_STREAM_SIZE_IN_video_in_V_data_V  "../tv/stream_size/stream_size_in_video_in_V_data_V.dat"
   #define AUTOTB_TVIN_video_in_V_keep_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_keep_V.dat"
   #define WRAPC_STREAM_SIZE_IN_video_in_V_keep_V  "../tv/stream_size/stream_size_in_video_in_V_keep_V.dat"
   #define AUTOTB_TVIN_video_in_V_strb_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_strb_V.dat"
   #define WRAPC_STREAM_SIZE_IN_video_in_V_strb_V  "../tv/stream_size/stream_size_in_video_in_V_strb_V.dat"
   #define AUTOTB_TVIN_video_in_V_user_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_user_V.dat"
   #define WRAPC_STREAM_SIZE_IN_video_in_V_user_V  "../tv/stream_size/stream_size_in_video_in_V_user_V.dat"
   #define AUTOTB_TVIN_video_in_V_last_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_last_V.dat"
   #define WRAPC_STREAM_SIZE_IN_video_in_V_last_V  "../tv/stream_size/stream_size_in_video_in_V_last_V.dat"
   #define AUTOTB_TVIN_video_in_V_id_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_id_V.dat"
   #define WRAPC_STREAM_SIZE_IN_video_in_V_id_V  "../tv/stream_size/stream_size_in_video_in_V_id_V.dat"
   #define AUTOTB_TVIN_video_in_V_dest_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_dest_V.dat"
   #define WRAPC_STREAM_SIZE_IN_video_in_V_dest_V  "../tv/stream_size/stream_size_in_video_in_V_dest_V.dat"
   #define AUTOTB_TVIN_fb_out  "../tv/cdatafile/c.RGB2yfb.autotvin_fb_out.dat"
   #define AUTOTB_TVIN_stride  "../tv/cdatafile/c.RGB2yfb.autotvin_stride.dat"
   #define AUTOTB_TVOUT_ap_return  "../tv/cdatafile/c.RGB2yfb.autotvout_ap_return.dat"
   #define INTER_TCL  "../tv/cdatafile/ref.tcl"

   #define AUTOTB_TVOUT_PC_gmem  "../tv/rtldatafile/rtl.RGB2yfb.autotvout_gmem.dat"
   #define AUTOTB_TVOUT_PC_ap_return  "../tv/rtldatafile/rtl.RGB2yfb.autotvout_ap_return.dat"

class AESL_TRANSACTION {
    public:
        AESL_TRANSACTION(const char* name) {
            mName = (char *)malloc((strlen(name)+1)*sizeof(char));            strcpy(mName,name);
        }
        ~AESL_TRANSACTION() {
            free(mName);        }
    public:
        FILE* mFile;
        char* mName;
};

class INTER_TCL_FILE {
    public:
//functions
        INTER_TCL_FILE(const char* name) {
            mName = name;
            gmem_depth = 0;
            video_in_V_data_V_depth = 0;
            video_in_V_keep_V_depth = 0;
            video_in_V_strb_V_depth = 0;
            video_in_V_user_V_depth = 0;
            video_in_V_last_V_depth = 0;
            video_in_V_id_V_depth = 0;
            video_in_V_dest_V_depth = 0;
            fb_out_depth = 0;
            stride_depth = 0;
            ap_return_depth = 0;
            trans_num =0;
        }
        ~INTER_TCL_FILE() {
            mFile.open(mName);
            if (!mFile.good() ) {
                cout<<"Failed to open file ref.tcl."<<endl;
                exit (1);
            }
            string total_list = get_depth_list();
            mFile<<"set depth_list {\n";
            mFile<<total_list; 
            mFile<<"}\n";
            mFile<<"set trans_num "<<trans_num<<endl;
            mFile.close();
        }
        string get_depth_list () {
            stringstream total_list;
            total_list<<"   {gmem "<< gmem_depth << "}\n";
            total_list<<"   {video_in_V_data_V "<< video_in_V_data_V_depth << "}\n";
            total_list<<"   {video_in_V_keep_V "<< video_in_V_keep_V_depth << "}\n";
            total_list<<"   {video_in_V_strb_V "<< video_in_V_strb_V_depth << "}\n";
            total_list<<"   {video_in_V_user_V "<< video_in_V_user_V_depth << "}\n";
            total_list<<"   {video_in_V_last_V "<< video_in_V_last_V_depth << "}\n";
            total_list<<"   {video_in_V_id_V "<< video_in_V_id_V_depth << "}\n";
            total_list<<"   {video_in_V_dest_V "<< video_in_V_dest_V_depth << "}\n";
            total_list<<"   {fb_out "<< fb_out_depth << "}\n";
            total_list<<"   {stride "<< stride_depth << "}\n";
            total_list<<"   {ap_return "<< ap_return_depth << "}\n";
            return total_list.str();
        }
        void set_num (int num , int* class_num) {
            (*class_num) = (*class_num) > num ? (*class_num) : num;
        }
    public:
//variables
        int gmem_depth;
        int video_in_V_data_V_depth;
        int video_in_V_keep_V_depth;
        int video_in_V_strb_V_depth;
        int video_in_V_user_V_depth;
        int video_in_V_last_V_depth;
        int video_in_V_id_V_depth;
        int video_in_V_dest_V_depth;
        int fb_out_depth;
        int stride_depth;
        int ap_return_depth;
        int trans_num;
    private:
        ofstream mFile;
        const char* mName;
};

class AESL_TRANSACTION_PC {
    public:
        AESL_TRANSACTION_PC(const char* name) {
            mName = (char *)malloc((strlen(name)+1)*sizeof(char));            strcpy(mName,name);
        }
        ~AESL_TRANSACTION_PC() {
            free(mName);        }
    public:
        fstream file_token;
        char * mName;
};

extern int RGB2yfb (
hls::stream<ap_axiu<24, 1, 1, 1 > > (&video_in),
long long unsigned int* fb_out,
unsigned int hsize,
unsigned int vsize,
unsigned int stride,
unsigned int fsize);
int AESL_WRAP_RGB2yfb (
hls::stream<ap_axiu<24, 1, 1, 1 > > (&video_in),
long long unsigned int* fb_out,
unsigned int hsize,
unsigned int vsize,
unsigned int stride,
unsigned int fsize) {

    fstream wrapc_switch_file_token;

    wrapc_switch_file_token.open(".hls_cosim_wrapc_switch.log");

    fstream wrapc_tv_switch_file_token;

    wrapc_tv_switch_file_token.open(".hls_cosim_wrapc_tv_switch.log");

    int AESL_i;

    if (wrapc_switch_file_token.good()) {

        static unsigned AESL_transaction_pc;

        string AESL_token;

        string AESL_num;

            FILE * communication_file;

            char get_com_str_set[4];

            char get_com_str[19];

            int get_com_int;

            do {

                do {

                    communication_file = fopen("com_wrapc_pc.tcl","r");

                } while (communication_file == NULL);

                fscanf(communication_file, "%s %s %d", get_com_str_set, get_com_str, &get_com_int);

                fclose(communication_file);

            } while (strcmp(get_com_str, "trans_num_wrapc_pc") != 0 || get_com_int < AESL_transaction_pc);

        static AESL_FILE_HANDLER aesl_fh;

        char str[100];

        char transaction_num_char[40];

        sprintf(transaction_num_char, "_%0d", AESL_transaction_pc);

        strcpy(str,AUTOTB_TVOUT_PC_gmem);

        strcat(str,transaction_num_char);

        AESL_TRANSACTION_PC tvout_pc_gmem(str);

        strcpy(str,AUTOTB_TVOUT_PC_ap_return);

        strcat(str,transaction_num_char);

        AESL_TRANSACTION_PC tvout_pc_ap_return(str);

        int AESL_return;

        aesl_fh.read(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, AESL_token); //[[transaction]]

        aesl_fh.read(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, AESL_num); //transaction number

        if (atoi(AESL_num.c_str()) == AESL_transaction_pc ) {

            aesl_fh.read(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, AESL_token); //pop_size

            int aesl_tmp_1 = atoi(AESL_token.c_str());

            for (int i = 0 ; i < aesl_tmp_1  ; i++) {

                video_in.read();

            }

            aesl_fh.read(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, AESL_token); //[[/transaction]]

        }

        tvout_pc_gmem.file_token.open(tvout_pc_gmem.mName);

        if (!tvout_pc_gmem.file_token.good()) {

            cout<<"Failed to open tv file ."<<tvout_pc_gmem.mName<<endl;

            exit (1);

        }

        tvout_pc_gmem.file_token >> AESL_token;//[[transaction]]

        if ( AESL_token != "[[transaction]]") {

           cout<<"Illegal file tvout_gmem format !"<<endl;

           exit(1);

        }

        tvout_pc_gmem.file_token >> AESL_num;//transaction number

        if (atoi(AESL_num.c_str()) == AESL_transaction_pc ) {

            tvout_pc_gmem.file_token >> AESL_token;//data

            sc_bv<64> *gmem_pc_buffer = new sc_bv<64>[57600];

            int i = 0;

            while (AESL_token != "[[/transaction]]") {

                bool no_x = false;

                bool err = false;

                while (!no_x) {

                size_t x_found = AESL_token.find('X');

                if (x_found != string::npos) {

                    if (!err) {

                        cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'gmem', possible cause: There are uninitialized variables in the C design." << endl; 

                        err = true;

                    }

                    AESL_token.replace(x_found, 1, "0");

                } else {

                    no_x = true;

                }

                }

                no_x = false;

                while (!no_x) {

                size_t x_found = AESL_token.find('x', 2);

                if (x_found != string::npos) {

                    if (!err) {

                        cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'gmem', possible cause: There are uninitialized variables in the C design." << endl; 

                        err = true;

                    }

                    AESL_token.replace(x_found, 1, "0");

                } else {

                    no_x = true;

                }

                }

                if (AESL_token != "") {

                    gmem_pc_buffer[i] = AESL_token.c_str();

                    i++;

                }

                tvout_pc_gmem.file_token >> AESL_token;//data or [[/transaction]]

                if (AESL_token == "[[[/runtime]]]" || tvout_pc_gmem.file_token.eof()) {

                   cout<<"Illegal file tvout_gmem format !"<<endl;

                   exit(1);

                }

            }

            tvout_pc_gmem.file_token.close();

            strcpy(str,AUTOTB_TVOUT_PC_gmem);

            strcat(str,transaction_num_char);

            remove(str);

            if (i > 0) {

                sc_lv<64>* fb_out_lv0_0_57599_1 = new sc_lv<64>[57600];

                AESL_i = 0;

                for (int i_0 = 0; i_0 <= 57599; i_0 += 1)

                {

                if(&(fb_out[0]) != NULL)

                {

                fb_out_lv0_0_57599_1[0 + AESL_i].range(63, 0) = sc_bv<64>(gmem_pc_buffer[0 + AESL_i].range(63, 0));

                }

                AESL_i++;

                }

                {//bitslice

                {//celement

                AESL_i = 0; //subscript for rtl array

                for (int i_0 = 0; i_0 <= 57599 ; i_0+= 1) {

                    if(&(fb_out[0]) != 0) {

                       fb_out[i_0] = (fb_out_lv0_0_57599_1[0 + AESL_i]).to_uint64();

                    }

                    AESL_i++;

                }

                }//celement

                }//bitslice

                }

            delete [] gmem_pc_buffer;

        }

        tvout_pc_ap_return.file_token.open(tvout_pc_ap_return.mName);

        if (!tvout_pc_ap_return.file_token.good()) {

            cout<<"Failed to open tv file ."<<tvout_pc_ap_return.mName<<endl;

            exit (1);

        }

        tvout_pc_ap_return.file_token >> AESL_token;//[[transaction]]

        if ( AESL_token != "[[transaction]]") {

           cout<<"Illegal file tvout_ap_return format !"<<endl;

           exit(1);

        }

        tvout_pc_ap_return.file_token >> AESL_num;//transaction number

        if (atoi(AESL_num.c_str()) == AESL_transaction_pc ) {

            tvout_pc_ap_return.file_token >> AESL_token;//data

            sc_bv<32> ap_return_pc_buffer;

            int i = 0;

            while (AESL_token != "[[/transaction]]") {

                bool no_x = false;

                bool err = false;

                while (!no_x) {

                size_t x_found = AESL_token.find('X');

                if (x_found != string::npos) {

                    if (!err) {

                        cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'ap_return', possible cause: There are uninitialized variables in the C design." << endl; 

                        err = true;

                    }

                    AESL_token.replace(x_found, 1, "0");

                } else {

                    no_x = true;

                }

                }

                no_x = false;

                while (!no_x) {

                size_t x_found = AESL_token.find('x', 2);

                if (x_found != string::npos) {

                    if (!err) {

                        cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'ap_return', possible cause: There are uninitialized variables in the C design." << endl; 

                        err = true;

                    }

                    AESL_token.replace(x_found, 1, "0");

                } else {

                    no_x = true;

                }

                }

                if (AESL_token != "") {

                    ap_return_pc_buffer = AESL_token.c_str();

                    i++;

                }

                tvout_pc_ap_return.file_token >> AESL_token;//data or [[/transaction]]

                if (AESL_token == "[[[/runtime]]]" || tvout_pc_ap_return.file_token.eof()) {

                   cout<<"Illegal file tvout_ap_return format !"<<endl;

                   exit(1);

                }

            }

            tvout_pc_ap_return.file_token.close();

            strcpy(str,AUTOTB_TVOUT_PC_ap_return);

            strcat(str,transaction_num_char);

            remove(str);

            if (i > 0) {

                sc_lv<32> return_lv0_0_1_0;

                AESL_i = 0;

                if(&(AESL_return) != NULL)

                {

                return_lv0_0_1_0.range(31, 0) = sc_bv<32>(ap_return_pc_buffer.range(31, 0));

                }

                AESL_i++;

                {//bitslice

                {//celement

                AESL_i = 0; //subscript for rtl array

                if(&(AESL_return) != 0) {

                   AESL_return = (return_lv0_0_1_0).to_uint64();

                }

                AESL_i++;

                }//celement

                }//bitslice

                }

        }

        AESL_transaction_pc ++ ;

        return AESL_return;

    } else if (wrapc_tv_switch_file_token.good()){

        static unsigned AESL_transaction;

        char transaction_num_char[40];

        sprintf(transaction_num_char, "_%0d", AESL_transaction);

        char file_name_char[100];

        strcpy(file_name_char, AUTOTB_TVOUT_gmem);

        strcat(file_name_char, transaction_num_char);

        AESL_TRANSACTION tvout_gmem(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_data_V);

        strcat(file_name_char, transaction_num_char);

        AESL_TRANSACTION tvin_video_in_V_data_V(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_keep_V);

        strcat(file_name_char, transaction_num_char);

        AESL_TRANSACTION tvin_video_in_V_keep_V(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_strb_V);

        strcat(file_name_char, transaction_num_char);

        AESL_TRANSACTION tvin_video_in_V_strb_V(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_user_V);

        strcat(file_name_char, transaction_num_char);

        AESL_TRANSACTION tvin_video_in_V_user_V(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_last_V);

        strcat(file_name_char, transaction_num_char);

        AESL_TRANSACTION tvin_video_in_V_last_V(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_id_V);

        strcat(file_name_char, transaction_num_char);

        AESL_TRANSACTION tvin_video_in_V_id_V(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_dest_V);

        strcat(file_name_char, transaction_num_char);

        AESL_TRANSACTION tvin_video_in_V_dest_V(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_fb_out);

        strcat(file_name_char, transaction_num_char);

        AESL_TRANSACTION tvin_fb_out(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_stride);

        strcat(file_name_char, transaction_num_char);

        AESL_TRANSACTION tvin_stride(file_name_char);

        strcpy(file_name_char, AUTOTB_TVOUT_ap_return);

        strcat(file_name_char, transaction_num_char);

        AESL_TRANSACTION tvout_ap_return(file_name_char);

        int leading_zero;

        std::vector<ap_axiu<24, 1, 1, 1 > > aesl_tmp_0;

        int aesl_tmp_1 = 0;

        while (!video_in.empty()) {

            aesl_tmp_0.push_back(video_in.read());

            aesl_tmp_1 ++;

        }

        tvin_fb_out.mFile = fopen(tvin_fb_out.mName, "w");

        if (tvin_fb_out.mFile == NULL) {

            cout<<"Failed to open tv file ."<<tvin_fb_out.mName<<endl;

            exit (1);

        }

        fprintf(tvin_fb_out.mFile, "[[transaction]] %d\n", AESL_transaction);

        sc_bv<32> fb_out_tvin_wrapc_buffer;

        for (int i = 0; i < 1 ; i++) {

            fprintf(tvin_fb_out.mFile, "%s\n", (fb_out_tvin_wrapc_buffer).to_string(SC_HEX).c_str());

        }

        fprintf(tvin_fb_out.mFile, "[[/transaction]] \n");

        fclose(tvin_fb_out.mFile);

        tvin_stride.mFile = fopen(tvin_stride.mName, "w");

        if (tvin_stride.mFile == NULL) {

            cout<<"Failed to open tv file ."<<tvin_stride.mName<<endl;

            exit (1);

        }

        fprintf(tvin_stride.mFile, "[[transaction]] %d\n", AESL_transaction);

        sc_bv<32> stride_tvin_wrapc_buffer;

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        sc_lv<32> stride_tmp_mem; 

        if(&(stride) != 0) {

        stride_tmp_mem = stride;

           stride_tvin_wrapc_buffer.range(31, 0) = stride_tmp_mem.range(31, 0 ) ;

        }

        AESL_i++;

        }//celement

        }//bitslice

        for (int i = 0; i < 1 ; i++) {

            fprintf(tvin_stride.mFile, "%s\n", (stride_tvin_wrapc_buffer).to_string(SC_HEX).c_str());

        }

        fprintf(tvin_stride.mFile, "[[/transaction]] \n");

        fclose(tvin_stride.mFile);

        for (int i = 0; i < aesl_tmp_1; i++) {

            video_in.write(aesl_tmp_0[i]);

        }

        int AESL_return = RGB2yfb(video_in,fb_out,hsize,vsize,stride,fsize);

        int aesl_tmp_2 = video_in.size();

        tvout_gmem.mFile = fopen(tvout_gmem.mName, "w");

        if (tvout_gmem.mFile == NULL) {

            cout<<"Failed to open tv file ."<<tvout_gmem.mName<<endl;

            exit (1);

        }

        fprintf(tvout_gmem.mFile, "[[transaction]] %d\n", AESL_transaction);

        sc_bv<64> *gmem_tvout_wrapc_buffer = new sc_bv<64>[57600];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= 57599 ; i_0+= 1) {

        sc_lv<64> fb_out_tmp_mem; 

            if(&(fb_out[0]) != 0) {

            fb_out_tmp_mem = fb_out[i_0];

               gmem_tvout_wrapc_buffer[0 + AESL_i].range(63, 0) = fb_out_tmp_mem.range(63, 0 ) ;

            }

            AESL_i++;

        }

        }//celement

        }//bitslice

        for (int i = 0; i < 57600 ; i++) {

            fprintf(tvout_gmem.mFile, "%s\n", (gmem_tvout_wrapc_buffer[i]).to_string(SC_HEX).c_str());

        }

        fprintf(tvout_gmem.mFile, "[[/transaction]] \n");

        fclose(tvout_gmem.mFile);

        delete [] gmem_tvout_wrapc_buffer;

        tvin_video_in_V_data_V.mFile = fopen(tvin_video_in_V_data_V.mName, "w");

        if (tvin_video_in_V_data_V.mFile == NULL) {

            cout<<"Failed to open tv file ."<<tvin_video_in_V_data_V.mName<<endl;

            exit (1);

        }

        fprintf(tvin_video_in_V_data_V.mFile, "[[transaction]] %d\n", AESL_transaction);

        sc_bv<24> *video_in_V_data_V_tvin_wrapc_buffer = new sc_bv<24>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<24> video_in_V_data_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<24> video_in_V_data_V_tmp_mem; 

                if(&(aesl_tmp_0[0].data) != 0) {

                video_in_V_data_V_tmp_mem = (aesl_tmp_0[i_0].data).to_string(2).c_str();

                   video_in_V_data_V_tvin_wrapc_buffer[0 + AESL_i].range(23, 0) = video_in_V_data_V_tmp_mem.range(23, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2 ; i++) {

            fprintf(tvin_video_in_V_data_V.mFile, "%s\n", (video_in_V_data_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());

        }

        fprintf(tvin_video_in_V_data_V.mFile, "[[/transaction]] \n");

        fclose(tvin_video_in_V_data_V.mFile);

        delete [] video_in_V_data_V_tvin_wrapc_buffer;

        tvin_video_in_V_keep_V.mFile = fopen(tvin_video_in_V_keep_V.mName, "w");

        if (tvin_video_in_V_keep_V.mFile == NULL) {

            cout<<"Failed to open tv file ."<<tvin_video_in_V_keep_V.mName<<endl;

            exit (1);

        }

        fprintf(tvin_video_in_V_keep_V.mFile, "[[transaction]] %d\n", AESL_transaction);

        sc_bv<3> *video_in_V_keep_V_tvin_wrapc_buffer = new sc_bv<3>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<3> video_in_V_keep_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<3> video_in_V_keep_V_tmp_mem; 

                if(&(aesl_tmp_0[0].keep) != 0) {

                video_in_V_keep_V_tmp_mem = (aesl_tmp_0[i_0].keep).to_string(2).c_str();

                   video_in_V_keep_V_tvin_wrapc_buffer[0 + AESL_i].range(2, 0) = video_in_V_keep_V_tmp_mem.range(2, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2 ; i++) {

            fprintf(tvin_video_in_V_keep_V.mFile, "%s\n", (video_in_V_keep_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());

        }

        fprintf(tvin_video_in_V_keep_V.mFile, "[[/transaction]] \n");

        fclose(tvin_video_in_V_keep_V.mFile);

        delete [] video_in_V_keep_V_tvin_wrapc_buffer;

        tvin_video_in_V_strb_V.mFile = fopen(tvin_video_in_V_strb_V.mName, "w");

        if (tvin_video_in_V_strb_V.mFile == NULL) {

            cout<<"Failed to open tv file ."<<tvin_video_in_V_strb_V.mName<<endl;

            exit (1);

        }

        fprintf(tvin_video_in_V_strb_V.mFile, "[[transaction]] %d\n", AESL_transaction);

        sc_bv<3> *video_in_V_strb_V_tvin_wrapc_buffer = new sc_bv<3>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<3> video_in_V_strb_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<3> video_in_V_strb_V_tmp_mem; 

                if(&(aesl_tmp_0[0].strb) != 0) {

                video_in_V_strb_V_tmp_mem = (aesl_tmp_0[i_0].strb).to_string(2).c_str();

                   video_in_V_strb_V_tvin_wrapc_buffer[0 + AESL_i].range(2, 0) = video_in_V_strb_V_tmp_mem.range(2, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2 ; i++) {

            fprintf(tvin_video_in_V_strb_V.mFile, "%s\n", (video_in_V_strb_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());

        }

        fprintf(tvin_video_in_V_strb_V.mFile, "[[/transaction]] \n");

        fclose(tvin_video_in_V_strb_V.mFile);

        delete [] video_in_V_strb_V_tvin_wrapc_buffer;

        tvin_video_in_V_user_V.mFile = fopen(tvin_video_in_V_user_V.mName, "w");

        if (tvin_video_in_V_user_V.mFile == NULL) {

            cout<<"Failed to open tv file ."<<tvin_video_in_V_user_V.mName<<endl;

            exit (1);

        }

        fprintf(tvin_video_in_V_user_V.mFile, "[[transaction]] %d\n", AESL_transaction);

        sc_bv<1> *video_in_V_user_V_tvin_wrapc_buffer = new sc_bv<1>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<1> video_in_V_user_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<1> video_in_V_user_V_tmp_mem; 

                if(&(aesl_tmp_0[0].user) != 0) {

                video_in_V_user_V_tmp_mem = (aesl_tmp_0[i_0].user).to_string(2).c_str();

                   video_in_V_user_V_tvin_wrapc_buffer[0 + AESL_i].range(0, 0) = video_in_V_user_V_tmp_mem.range(0, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2 ; i++) {

            fprintf(tvin_video_in_V_user_V.mFile, "%s\n", (video_in_V_user_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());

        }

        fprintf(tvin_video_in_V_user_V.mFile, "[[/transaction]] \n");

        fclose(tvin_video_in_V_user_V.mFile);

        delete [] video_in_V_user_V_tvin_wrapc_buffer;

        tvin_video_in_V_last_V.mFile = fopen(tvin_video_in_V_last_V.mName, "w");

        if (tvin_video_in_V_last_V.mFile == NULL) {

            cout<<"Failed to open tv file ."<<tvin_video_in_V_last_V.mName<<endl;

            exit (1);

        }

        fprintf(tvin_video_in_V_last_V.mFile, "[[transaction]] %d\n", AESL_transaction);

        sc_bv<1> *video_in_V_last_V_tvin_wrapc_buffer = new sc_bv<1>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<1> video_in_V_last_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<1> video_in_V_last_V_tmp_mem; 

                if(&(aesl_tmp_0[0].last) != 0) {

                video_in_V_last_V_tmp_mem = (aesl_tmp_0[i_0].last).to_string(2).c_str();

                   video_in_V_last_V_tvin_wrapc_buffer[0 + AESL_i].range(0, 0) = video_in_V_last_V_tmp_mem.range(0, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2 ; i++) {

            fprintf(tvin_video_in_V_last_V.mFile, "%s\n", (video_in_V_last_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());

        }

        fprintf(tvin_video_in_V_last_V.mFile, "[[/transaction]] \n");

        fclose(tvin_video_in_V_last_V.mFile);

        delete [] video_in_V_last_V_tvin_wrapc_buffer;

        tvin_video_in_V_id_V.mFile = fopen(tvin_video_in_V_id_V.mName, "w");

        if (tvin_video_in_V_id_V.mFile == NULL) {

            cout<<"Failed to open tv file ."<<tvin_video_in_V_id_V.mName<<endl;

            exit (1);

        }

        fprintf(tvin_video_in_V_id_V.mFile, "[[transaction]] %d\n", AESL_transaction);

        sc_bv<1> *video_in_V_id_V_tvin_wrapc_buffer = new sc_bv<1>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<1> video_in_V_id_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<1> video_in_V_id_V_tmp_mem; 

                if(&(aesl_tmp_0[0].id) != 0) {

                video_in_V_id_V_tmp_mem = (aesl_tmp_0[i_0].id).to_string(2).c_str();

                   video_in_V_id_V_tvin_wrapc_buffer[0 + AESL_i].range(0, 0) = video_in_V_id_V_tmp_mem.range(0, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2 ; i++) {

            fprintf(tvin_video_in_V_id_V.mFile, "%s\n", (video_in_V_id_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());

        }

        fprintf(tvin_video_in_V_id_V.mFile, "[[/transaction]] \n");

        fclose(tvin_video_in_V_id_V.mFile);

        delete [] video_in_V_id_V_tvin_wrapc_buffer;

        tvin_video_in_V_dest_V.mFile = fopen(tvin_video_in_V_dest_V.mName, "w");

        if (tvin_video_in_V_dest_V.mFile == NULL) {

            cout<<"Failed to open tv file ."<<tvin_video_in_V_dest_V.mName<<endl;

            exit (1);

        }

        fprintf(tvin_video_in_V_dest_V.mFile, "[[transaction]] %d\n", AESL_transaction);

        sc_bv<1> *video_in_V_dest_V_tvin_wrapc_buffer = new sc_bv<1>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<1> video_in_V_dest_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<1> video_in_V_dest_V_tmp_mem; 

                if(&(aesl_tmp_0[0].dest) != 0) {

                video_in_V_dest_V_tmp_mem = (aesl_tmp_0[i_0].dest).to_string(2).c_str();

                   video_in_V_dest_V_tvin_wrapc_buffer[0 + AESL_i].range(0, 0) = video_in_V_dest_V_tmp_mem.range(0, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2 ; i++) {

            fprintf(tvin_video_in_V_dest_V.mFile, "%s\n", (video_in_V_dest_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());

        }

        fprintf(tvin_video_in_V_dest_V.mFile, "[[/transaction]] \n");

        fclose(tvin_video_in_V_dest_V.mFile);

        delete [] video_in_V_dest_V_tvin_wrapc_buffer;

        tvout_ap_return.mFile = fopen(tvout_ap_return.mName, "w");

        if (tvout_ap_return.mFile == NULL) {

            cout<<"Failed to open tv file ."<<tvout_ap_return.mName<<endl;

            exit (1);

        }

        fprintf(tvout_ap_return.mFile, "[[transaction]] %d\n", AESL_transaction);

        sc_bv<32> ap_return_tvout_wrapc_buffer;

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        sc_lv<32> return_tmp_mem; 

        if(&(AESL_return) != 0) {

        return_tmp_mem = AESL_return;

           ap_return_tvout_wrapc_buffer.range(31, 0) = return_tmp_mem.range(31, 0 ) ;

        }

        AESL_i++;

        }//celement

        }//bitslice

        for (int i = 0; i < 1 ; i++) {

            fprintf(tvout_ap_return.mFile, "%s\n", (ap_return_tvout_wrapc_buffer).to_string(SC_HEX).c_str());

        }

        fprintf(tvout_ap_return.mFile, "[[/transaction]] \n");

        fclose(tvout_ap_return.mFile);
        FILE* communication_file;
        do {
            communication_file = fopen("com_wrapc.tcl", "w");
        } while (communication_file == NULL);
        fprintf(communication_file, "set trans_num_wrapc %d \n\n", AESL_transaction);
        fclose(communication_file);
    if(AESL_transaction > 0) {
        FILE * communication_file;
        char get_com_str_set[4];
        char get_com_str[14];
        int get_com_int;
        do {
            do {
                communication_file = fopen("com_rtl_ready.tcl","r");
            } while (communication_file == NULL);
            fscanf(communication_file, "%s %s %d", get_com_str_set, get_com_str, &get_com_int);
            fclose(communication_file);
        } while (strcmp(get_com_str, "trans_num_rtl") != 0 || get_com_int < AESL_transaction);
    }

        if(AESL_transaction > 0) {

        sprintf(transaction_num_char, "_%0d", AESL_transaction - 1);

        strcpy(file_name_char, AUTOTB_TVOUT_gmem);

        strcat(file_name_char, transaction_num_char);

        remove(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_data_V);

        strcat(file_name_char, transaction_num_char);

        remove(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_keep_V);

        strcat(file_name_char, transaction_num_char);

        remove(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_strb_V);

        strcat(file_name_char, transaction_num_char);

        remove(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_user_V);

        strcat(file_name_char, transaction_num_char);

        remove(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_last_V);

        strcat(file_name_char, transaction_num_char);

        remove(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_id_V);

        strcat(file_name_char, transaction_num_char);

        remove(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_video_in_V_dest_V);

        strcat(file_name_char, transaction_num_char);

        remove(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_fb_out);

        strcat(file_name_char, transaction_num_char);

        remove(file_name_char);

        strcpy(file_name_char, AUTOTB_TVIN_stride);

        strcat(file_name_char, transaction_num_char);

        remove(file_name_char);

        strcpy(file_name_char, AUTOTB_TVOUT_ap_return);

        strcat(file_name_char, transaction_num_char);

        remove(file_name_char);
}

        AESL_transaction++;

        return AESL_return;

    } else {

        static unsigned AESL_transaction;

        static AESL_FILE_HANDLER aesl_fh;

        char* wrapc_stream_size_in_video_in_V_data_V = new char[50];

        char* wrapc_stream_size_in_video_in_V_keep_V = new char[50];

        char* wrapc_stream_size_in_video_in_V_strb_V = new char[50];

        char* wrapc_stream_size_in_video_in_V_user_V = new char[50];

        char* wrapc_stream_size_in_video_in_V_last_V = new char[50];

        char* wrapc_stream_size_in_video_in_V_id_V = new char[50];

        char* wrapc_stream_size_in_video_in_V_dest_V = new char[50];

        static INTER_TCL_FILE tcl_file(INTER_TCL);


        int leading_zero;

        std::vector<ap_axiu<24, 1, 1, 1 > > aesl_tmp_0;

        int aesl_tmp_1 = 0;

        while (!video_in.empty()) {

            aesl_tmp_0.push_back(video_in.read());

            aesl_tmp_1 ++;

        }

        sc_bv<64> *gmem_tvin_wrapc_buffer = new sc_bv<64>[57600];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= 57599 ; i_0+= 1) {

        sc_lv<64> fb_out_tmp_mem; 

            if(&(fb_out[0]) != 0) {

            fb_out_tmp_mem = fb_out[i_0];

               gmem_tvin_wrapc_buffer[0 + AESL_i].range(63, 0) = fb_out_tmp_mem.range(63, 0 ) ;

            }

            AESL_i++;

        }

        }//celement

        }//bitslice

        tcl_file.set_num(57600,&tcl_file.gmem_depth);

        delete [] gmem_tvin_wrapc_buffer;

        sc_bv<32> fb_out_tvin_wrapc_buffer;

        tcl_file.set_num(1,&tcl_file.fb_out_depth);

        sc_bv<32> stride_tvin_wrapc_buffer;

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        sc_lv<32> stride_tmp_mem; 

        if(&(stride) != 0) {

        stride_tmp_mem = stride;

           stride_tvin_wrapc_buffer.range(31, 0) = stride_tmp_mem.range(31, 0 ) ;

        }

        AESL_i++;

        }//celement

        }//bitslice

        tcl_file.set_num(1,&tcl_file.stride_depth);

        for (int i = 0; i < aesl_tmp_1; i++) {

            video_in.write(aesl_tmp_0[i]);

        }

        int AESL_return = RGB2yfb(video_in,fb_out,hsize,vsize,stride,fsize);

        int aesl_tmp_2 = video_in.size();

        sc_bv<64> *gmem_tvout_wrapc_buffer = new sc_bv<64>[57600];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= 57599 ; i_0+= 1) {

        sc_lv<64> fb_out_tmp_mem; 

            if(&(fb_out[0]) != 0) {

            fb_out_tmp_mem = fb_out[i_0];

               gmem_tvout_wrapc_buffer[0 + AESL_i].range(63, 0) = fb_out_tmp_mem.range(63, 0 ) ;

            }

            AESL_i++;

        }

        }//celement

        }//bitslice

        tcl_file.set_num(57600,&tcl_file.gmem_depth);

        delete [] gmem_tvout_wrapc_buffer;

        sc_bv<24> *video_in_V_data_V_tvin_wrapc_buffer = new sc_bv<24>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<24> video_in_V_data_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<24> video_in_V_data_V_tmp_mem; 

                if(&(aesl_tmp_0[0].data) != 0) {

                video_in_V_data_V_tmp_mem = (aesl_tmp_0[i_0].data).to_string(2).c_str();

                   video_in_V_data_V_tvin_wrapc_buffer[0 + AESL_i].range(23, 0) = video_in_V_data_V_tmp_mem.range(23, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2,&tcl_file.video_in_V_data_V_depth);

        delete [] video_in_V_data_V_tvin_wrapc_buffer;

        sprintf(wrapc_stream_size_in_video_in_V_data_V, "[[transaction]] %d\n", AESL_transaction);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, wrapc_stream_size_in_video_in_V_data_V);

        sprintf(wrapc_stream_size_in_video_in_V_data_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, wrapc_stream_size_in_video_in_V_data_V);

        sprintf(wrapc_stream_size_in_video_in_V_data_V, "[[/transaction]] \n");

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, wrapc_stream_size_in_video_in_V_data_V);

        sc_bv<3> *video_in_V_keep_V_tvin_wrapc_buffer = new sc_bv<3>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<3> video_in_V_keep_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<3> video_in_V_keep_V_tmp_mem; 

                if(&(aesl_tmp_0[0].keep) != 0) {

                video_in_V_keep_V_tmp_mem = (aesl_tmp_0[i_0].keep).to_string(2).c_str();

                   video_in_V_keep_V_tvin_wrapc_buffer[0 + AESL_i].range(2, 0) = video_in_V_keep_V_tmp_mem.range(2, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2,&tcl_file.video_in_V_keep_V_depth);

        delete [] video_in_V_keep_V_tvin_wrapc_buffer;

        sprintf(wrapc_stream_size_in_video_in_V_keep_V, "[[transaction]] %d\n", AESL_transaction);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_keep_V, wrapc_stream_size_in_video_in_V_keep_V);

        sprintf(wrapc_stream_size_in_video_in_V_keep_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_keep_V, wrapc_stream_size_in_video_in_V_keep_V);

        sprintf(wrapc_stream_size_in_video_in_V_keep_V, "[[/transaction]] \n");

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_keep_V, wrapc_stream_size_in_video_in_V_keep_V);

        sc_bv<3> *video_in_V_strb_V_tvin_wrapc_buffer = new sc_bv<3>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<3> video_in_V_strb_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<3> video_in_V_strb_V_tmp_mem; 

                if(&(aesl_tmp_0[0].strb) != 0) {

                video_in_V_strb_V_tmp_mem = (aesl_tmp_0[i_0].strb).to_string(2).c_str();

                   video_in_V_strb_V_tvin_wrapc_buffer[0 + AESL_i].range(2, 0) = video_in_V_strb_V_tmp_mem.range(2, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2,&tcl_file.video_in_V_strb_V_depth);

        delete [] video_in_V_strb_V_tvin_wrapc_buffer;

        sprintf(wrapc_stream_size_in_video_in_V_strb_V, "[[transaction]] %d\n", AESL_transaction);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_strb_V, wrapc_stream_size_in_video_in_V_strb_V);

        sprintf(wrapc_stream_size_in_video_in_V_strb_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_strb_V, wrapc_stream_size_in_video_in_V_strb_V);

        sprintf(wrapc_stream_size_in_video_in_V_strb_V, "[[/transaction]] \n");

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_strb_V, wrapc_stream_size_in_video_in_V_strb_V);

        sc_bv<1> *video_in_V_user_V_tvin_wrapc_buffer = new sc_bv<1>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<1> video_in_V_user_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<1> video_in_V_user_V_tmp_mem; 

                if(&(aesl_tmp_0[0].user) != 0) {

                video_in_V_user_V_tmp_mem = (aesl_tmp_0[i_0].user).to_string(2).c_str();

                   video_in_V_user_V_tvin_wrapc_buffer[0 + AESL_i].range(0, 0) = video_in_V_user_V_tmp_mem.range(0, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2,&tcl_file.video_in_V_user_V_depth);

        delete [] video_in_V_user_V_tvin_wrapc_buffer;

        sprintf(wrapc_stream_size_in_video_in_V_user_V, "[[transaction]] %d\n", AESL_transaction);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_user_V, wrapc_stream_size_in_video_in_V_user_V);

        sprintf(wrapc_stream_size_in_video_in_V_user_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_user_V, wrapc_stream_size_in_video_in_V_user_V);

        sprintf(wrapc_stream_size_in_video_in_V_user_V, "[[/transaction]] \n");

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_user_V, wrapc_stream_size_in_video_in_V_user_V);

        sc_bv<1> *video_in_V_last_V_tvin_wrapc_buffer = new sc_bv<1>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<1> video_in_V_last_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<1> video_in_V_last_V_tmp_mem; 

                if(&(aesl_tmp_0[0].last) != 0) {

                video_in_V_last_V_tmp_mem = (aesl_tmp_0[i_0].last).to_string(2).c_str();

                   video_in_V_last_V_tvin_wrapc_buffer[0 + AESL_i].range(0, 0) = video_in_V_last_V_tmp_mem.range(0, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2,&tcl_file.video_in_V_last_V_depth);

        delete [] video_in_V_last_V_tvin_wrapc_buffer;

        sprintf(wrapc_stream_size_in_video_in_V_last_V, "[[transaction]] %d\n", AESL_transaction);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_last_V, wrapc_stream_size_in_video_in_V_last_V);

        sprintf(wrapc_stream_size_in_video_in_V_last_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_last_V, wrapc_stream_size_in_video_in_V_last_V);

        sprintf(wrapc_stream_size_in_video_in_V_last_V, "[[/transaction]] \n");

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_last_V, wrapc_stream_size_in_video_in_V_last_V);

        sc_bv<1> *video_in_V_id_V_tvin_wrapc_buffer = new sc_bv<1>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<1> video_in_V_id_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<1> video_in_V_id_V_tmp_mem; 

                if(&(aesl_tmp_0[0].id) != 0) {

                video_in_V_id_V_tmp_mem = (aesl_tmp_0[i_0].id).to_string(2).c_str();

                   video_in_V_id_V_tvin_wrapc_buffer[0 + AESL_i].range(0, 0) = video_in_V_id_V_tmp_mem.range(0, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2,&tcl_file.video_in_V_id_V_depth);

        delete [] video_in_V_id_V_tvin_wrapc_buffer;

        sprintf(wrapc_stream_size_in_video_in_V_id_V, "[[transaction]] %d\n", AESL_transaction);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_id_V, wrapc_stream_size_in_video_in_V_id_V);

        sprintf(wrapc_stream_size_in_video_in_V_id_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_id_V, wrapc_stream_size_in_video_in_V_id_V);

        sprintf(wrapc_stream_size_in_video_in_V_id_V, "[[/transaction]] \n");

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_id_V, wrapc_stream_size_in_video_in_V_id_V);

        sc_bv<1> *video_in_V_dest_V_tvin_wrapc_buffer = new sc_bv<1>[aesl_tmp_1 - aesl_tmp_2];

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1 ; i_0+= 1) {

        sc_lv<1> video_in_V_dest_V_tmp_mem; 

            for (int i_1 = 0; i_1 <= 0 ; i_1+= 1) {

            sc_lv<1> video_in_V_dest_V_tmp_mem; 

                if(&(aesl_tmp_0[0].dest) != 0) {

                video_in_V_dest_V_tmp_mem = (aesl_tmp_0[i_0].dest).to_string(2).c_str();

                   video_in_V_dest_V_tvin_wrapc_buffer[0 + AESL_i].range(0, 0) = video_in_V_dest_V_tmp_mem.range(0, 0 ) ;

                }

                AESL_i++;

            }

        }

        }//celement

        }//bitslice

        tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2,&tcl_file.video_in_V_dest_V_depth);

        delete [] video_in_V_dest_V_tvin_wrapc_buffer;

        sprintf(wrapc_stream_size_in_video_in_V_dest_V, "[[transaction]] %d\n", AESL_transaction);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_dest_V, wrapc_stream_size_in_video_in_V_dest_V);

        sprintf(wrapc_stream_size_in_video_in_V_dest_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_dest_V, wrapc_stream_size_in_video_in_V_dest_V);

        sprintf(wrapc_stream_size_in_video_in_V_dest_V, "[[/transaction]] \n");

        aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_dest_V, wrapc_stream_size_in_video_in_V_dest_V);

        sc_bv<32> ap_return_tvout_wrapc_buffer;

        {//bitslice

        {//celement

        AESL_i = 0; //subscript for rtl array

        sc_lv<32> return_tmp_mem; 

        if(&(AESL_return) != 0) {

        return_tmp_mem = AESL_return;

           ap_return_tvout_wrapc_buffer.range(31, 0) = return_tmp_mem.range(31, 0 ) ;

        }

        AESL_i++;

        }//celement

        }//bitslice

        tcl_file.set_num(1,&tcl_file.ap_return_depth);

        delete [] wrapc_stream_size_in_video_in_V_data_V;

        delete [] wrapc_stream_size_in_video_in_V_keep_V;

        delete [] wrapc_stream_size_in_video_in_V_strb_V;

        delete [] wrapc_stream_size_in_video_in_V_user_V;

        delete [] wrapc_stream_size_in_video_in_V_last_V;

        delete [] wrapc_stream_size_in_video_in_V_id_V;

        delete [] wrapc_stream_size_in_video_in_V_dest_V;

        AESL_transaction++;

        tcl_file.set_num(AESL_transaction , &tcl_file.trans_num);

        return AESL_return;

    }
}

