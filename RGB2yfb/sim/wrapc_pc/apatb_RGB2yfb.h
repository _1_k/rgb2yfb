// ==============================================================
// File generated on Mon Feb 11 21:25:05 +0900 2019
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================

extern int AESL_WRAP_RGB2yfb (
hls::stream<struct ap_axiu<24, 1, 1, 1 > > (&video_in),
long long unsigned int* fb_out,
unsigned int hsize,
unsigned int vsize,
unsigned int stride,
unsigned int fsize);
