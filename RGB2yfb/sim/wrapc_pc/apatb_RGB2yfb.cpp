// ==============================================================
// File generated on Mon Feb 11 21:25:04 +0900 2019
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================

#define AP_INT_MAX_W 32678

#include <systemc>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <stdint.h>
#include "SysCFileHandler.h"
#include "ap_int.h"
#include "ap_fixed.h"
#include <complex>
#include <stdbool.h>
#include "autopilot_cbe.h"
#include "hls_stream.h"
#include "hls_half.h"
#include "hls_signal_handler.h"

using namespace std;
using namespace sc_core;
using namespace sc_dt;


// [dump_struct_tree [build_nameSpaceTree] dumpedStructList] ---------->
    template< int D ,int U ,int TI ,int TD > struct ap_axiu;
    template<> struct ap_axiu<24, 1, 1, 1> {
        ap_uint<24> data;
        ap_uint<3> keep;
        ap_uint<3> strb;
        ap_uint<1> user;
        ap_uint<1> last;
        ap_uint<1> id;
        ap_uint<1> dest;
       } ;



// [dump_enumeration [get_enumeration_list]] ---------->


// wrapc file define: "gmem"
#define AUTOTB_TVOUT_gmem  "../tv/cdatafile/c.RGB2yfb.autotvout_gmem.dat"
#define AUTOTB_TVIN_gmem  "../tv/cdatafile/c.RGB2yfb.autotvin_gmem.dat"
// wrapc file define: "video_in_V_data_V"
#define AUTOTB_TVIN_video_in_V_data_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_data_V.dat"
#define WRAPC_STREAM_SIZE_IN_video_in_V_data_V  "../tv/stream_size/stream_size_in_video_in_V_data_V.dat"
#define WRAPC_STREAM_INGRESS_STATUS_video_in_V_data_V  "../tv/stream_size/stream_ingress_status_video_in_V_data_V.dat"
// wrapc file define: "video_in_V_keep_V"
#define AUTOTB_TVIN_video_in_V_keep_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_keep_V.dat"
#define WRAPC_STREAM_SIZE_IN_video_in_V_keep_V  "../tv/stream_size/stream_size_in_video_in_V_keep_V.dat"
#define WRAPC_STREAM_INGRESS_STATUS_video_in_V_keep_V  "../tv/stream_size/stream_ingress_status_video_in_V_keep_V.dat"
// wrapc file define: "video_in_V_strb_V"
#define AUTOTB_TVIN_video_in_V_strb_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_strb_V.dat"
#define WRAPC_STREAM_SIZE_IN_video_in_V_strb_V  "../tv/stream_size/stream_size_in_video_in_V_strb_V.dat"
#define WRAPC_STREAM_INGRESS_STATUS_video_in_V_strb_V  "../tv/stream_size/stream_ingress_status_video_in_V_strb_V.dat"
// wrapc file define: "video_in_V_user_V"
#define AUTOTB_TVIN_video_in_V_user_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_user_V.dat"
#define WRAPC_STREAM_SIZE_IN_video_in_V_user_V  "../tv/stream_size/stream_size_in_video_in_V_user_V.dat"
#define WRAPC_STREAM_INGRESS_STATUS_video_in_V_user_V  "../tv/stream_size/stream_ingress_status_video_in_V_user_V.dat"
// wrapc file define: "video_in_V_last_V"
#define AUTOTB_TVIN_video_in_V_last_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_last_V.dat"
#define WRAPC_STREAM_SIZE_IN_video_in_V_last_V  "../tv/stream_size/stream_size_in_video_in_V_last_V.dat"
#define WRAPC_STREAM_INGRESS_STATUS_video_in_V_last_V  "../tv/stream_size/stream_ingress_status_video_in_V_last_V.dat"
// wrapc file define: "video_in_V_id_V"
#define AUTOTB_TVIN_video_in_V_id_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_id_V.dat"
#define WRAPC_STREAM_SIZE_IN_video_in_V_id_V  "../tv/stream_size/stream_size_in_video_in_V_id_V.dat"
#define WRAPC_STREAM_INGRESS_STATUS_video_in_V_id_V  "../tv/stream_size/stream_ingress_status_video_in_V_id_V.dat"
// wrapc file define: "video_in_V_dest_V"
#define AUTOTB_TVIN_video_in_V_dest_V  "../tv/cdatafile/c.RGB2yfb.autotvin_video_in_V_dest_V.dat"
#define WRAPC_STREAM_SIZE_IN_video_in_V_dest_V  "../tv/stream_size/stream_size_in_video_in_V_dest_V.dat"
#define WRAPC_STREAM_INGRESS_STATUS_video_in_V_dest_V  "../tv/stream_size/stream_ingress_status_video_in_V_dest_V.dat"
// wrapc file define: "fb_out"
#define AUTOTB_TVIN_fb_out  "../tv/cdatafile/c.RGB2yfb.autotvin_fb_out.dat"
// wrapc file define: "stride"
#define AUTOTB_TVIN_stride  "../tv/cdatafile/c.RGB2yfb.autotvin_stride.dat"
// wrapc file define: "ap_return"
#define AUTOTB_TVOUT_ap_return  "../tv/cdatafile/c.RGB2yfb.autotvout_ap_return.dat"

#define INTER_TCL  "../tv/cdatafile/ref.tcl"

// tvout file define: "gmem"
#define AUTOTB_TVOUT_PC_gmem  "../tv/rtldatafile/rtl.RGB2yfb.autotvout_gmem.dat"
// tvout file define: "ap_return"
#define AUTOTB_TVOUT_PC_ap_return  "../tv/rtldatafile/rtl.RGB2yfb.autotvout_ap_return.dat"

class INTER_TCL_FILE {
	public:
		INTER_TCL_FILE(const char* name) {
			mName = name;
			gmem_depth = 0;
			video_in_V_data_V_depth = 0;
			video_in_V_keep_V_depth = 0;
			video_in_V_strb_V_depth = 0;
			video_in_V_user_V_depth = 0;
			video_in_V_last_V_depth = 0;
			video_in_V_id_V_depth = 0;
			video_in_V_dest_V_depth = 0;
			fb_out_depth = 0;
			stride_depth = 0;
			ap_return_depth = 0;
			trans_num =0;
		}

		~INTER_TCL_FILE() {
			mFile.open(mName);
			if (!mFile.good()) {
				cout << "Failed to open file ref.tcl" << endl;
				exit (1);
			}
			string total_list = get_depth_list();
			mFile << "set depth_list {\n";
			mFile << total_list;
			mFile << "}\n";
			mFile << "set trans_num "<<trans_num<<endl;
			mFile.close();
		}

		string get_depth_list () {
			stringstream total_list;
			total_list << "{gmem " << gmem_depth << "}\n";
			total_list << "{video_in_V_data_V " << video_in_V_data_V_depth << "}\n";
			total_list << "{video_in_V_keep_V " << video_in_V_keep_V_depth << "}\n";
			total_list << "{video_in_V_strb_V " << video_in_V_strb_V_depth << "}\n";
			total_list << "{video_in_V_user_V " << video_in_V_user_V_depth << "}\n";
			total_list << "{video_in_V_last_V " << video_in_V_last_V_depth << "}\n";
			total_list << "{video_in_V_id_V " << video_in_V_id_V_depth << "}\n";
			total_list << "{video_in_V_dest_V " << video_in_V_dest_V_depth << "}\n";
			total_list << "{fb_out " << fb_out_depth << "}\n";
			total_list << "{stride " << stride_depth << "}\n";
			total_list << "{ap_return " << ap_return_depth << "}\n";
			return total_list.str();
		}

		void set_num (int num , int* class_num) {
			(*class_num) = (*class_num) > num ? (*class_num) : num;
		}
	public:
		int gmem_depth;
		int video_in_V_data_V_depth;
		int video_in_V_keep_V_depth;
		int video_in_V_strb_V_depth;
		int video_in_V_user_V_depth;
		int video_in_V_last_V_depth;
		int video_in_V_id_V_depth;
		int video_in_V_dest_V_depth;
		int fb_out_depth;
		int stride_depth;
		int ap_return_depth;
		int trans_num;

	private:
		ofstream mFile;
		const char* mName;
};

extern int RGB2yfb (
hls::stream<ap_axiu<24, 1, 1, 1 > > (&video_in),
long long unsigned int* fb_out,
unsigned int hsize,
unsigned int vsize,
unsigned int stride,
unsigned int fsize);

int AESL_WRAP_RGB2yfb (
hls::stream<ap_axiu<24, 1, 1, 1 > > (&video_in),
long long unsigned int* fb_out,
unsigned int hsize,
unsigned int vsize,
unsigned int stride,
unsigned int fsize)
{
	refine_signal_handler();
	fstream wrapc_switch_file_token;
	wrapc_switch_file_token.open(".hls_cosim_wrapc_switch.log");
	int AESL_i;
	if (wrapc_switch_file_token.good())
	{
		CodeState = ENTER_WRAPC_PC;
		static unsigned AESL_transaction_pc = 0;
		string AESL_token;
		string AESL_num;
		static AESL_FILE_HANDLER aesl_fh;

		int AESL_return;
		// pop stream input: "video_in"
		aesl_fh.read(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, AESL_token); // [[transaction]]
		aesl_fh.read(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, AESL_token); // pop_size
			int aesl_tmp_1 = atoi(AESL_token.c_str());
			for (int i = 0; i < aesl_tmp_1; i++)
			{
				video_in.read();
			}
			aesl_fh.read(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, AESL_token); // [[/transaction]]
		}

		// output port post check: "gmem"
		aesl_fh.read(AUTOTB_TVOUT_PC_gmem, AESL_token); // [[transaction]]
		if (AESL_token != "[[transaction]]")
		{
			exit(1);
		}
		aesl_fh.read(AUTOTB_TVOUT_PC_gmem, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(AUTOTB_TVOUT_PC_gmem, AESL_token); // data

			sc_bv<64> *gmem_pc_buffer = new sc_bv<64>[57600];
			int i = 0;

			while (AESL_token != "[[/transaction]]")
			{
				bool no_x = false;
				bool err = false;

				// search and replace 'X' with "0" from the 1st char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('X');
					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'gmem', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				no_x = false;

				// search and replace 'x' with "0" from the 3rd char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('x', 2);

					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'gmem', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				// push token into output port buffer
				if (AESL_token != "")
				{
					gmem_pc_buffer[i] = AESL_token.c_str();
					i++;
				}

				aesl_fh.read(AUTOTB_TVOUT_PC_gmem, AESL_token); // data or [[/transaction]]

				if (AESL_token == "[[[/runtime]]]" || aesl_fh.eof(AUTOTB_TVOUT_PC_gmem))
				{
					exit(1);
				}
			}

			// ***********************************
			if (i > 0)
			{
				// RTL Name: gmem
				{
					// bitslice(63, 0)
					// {
						// celement: fb_out(63, 0)
						// {
							sc_lv<64>* fb_out_lv0_0_57599_1 = new sc_lv<64>[57600];
						// }
					// }

					// bitslice(63, 0)
					{
						int hls_map_index = 0;
						// celement: fb_out(63, 0)
						{
							// carray: (0) => (57599) @ (1)
							for (int i_0 = 0; i_0 <= 57599; i_0 += 1)
							{
								if (&(fb_out[0]) != NULL) // check the null address if the c port is array or others
								{
									fb_out_lv0_0_57599_1[hls_map_index].range(63, 0) = sc_bv<64>(gmem_pc_buffer[hls_map_index].range(63, 0));
									hls_map_index++;
								}
							}
						}
					}

					// bitslice(63, 0)
					{
						int hls_map_index = 0;
						// celement: fb_out(63, 0)
						{
							// carray: (0) => (57599) @ (1)
							for (int i_0 = 0; i_0 <= 57599; i_0 += 1)
							{
								// sub                    : i_0
								// ori_name               : fb_out[i_0]
								// sub_1st_elem           : 0
								// ori_name_1st_elem      : fb_out[0]
								// output_left_conversion : fb_out[i_0]
								// output_type_conversion : (fb_out_lv0_0_57599_1[hls_map_index]).to_uint64()
								if (&(fb_out[0]) != NULL) // check the null address if the c port is array or others
								{
									fb_out[i_0] = (fb_out_lv0_0_57599_1[hls_map_index]).to_uint64();
									hls_map_index++;
								}
							}
						}
					}
				}
			}

			// release memory allocation
			delete [] gmem_pc_buffer;
		}

		// output port post check: "ap_return"
		aesl_fh.read(AUTOTB_TVOUT_PC_ap_return, AESL_token); // [[transaction]]
		if (AESL_token != "[[transaction]]")
		{
			exit(1);
		}
		aesl_fh.read(AUTOTB_TVOUT_PC_ap_return, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(AUTOTB_TVOUT_PC_ap_return, AESL_token); // data

			sc_bv<32> ap_return_pc_buffer;
			int i = 0;

			while (AESL_token != "[[/transaction]]")
			{
				bool no_x = false;
				bool err = false;

				// search and replace 'X' with "0" from the 1st char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('X');
					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'ap_return', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				no_x = false;

				// search and replace 'x' with "0" from the 3rd char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('x', 2);

					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'ap_return', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				// push token into output port buffer
				if (AESL_token != "")
				{
					ap_return_pc_buffer = AESL_token.c_str();
					i++;
				}

				aesl_fh.read(AUTOTB_TVOUT_PC_ap_return, AESL_token); // data or [[/transaction]]

				if (AESL_token == "[[[/runtime]]]" || aesl_fh.eof(AUTOTB_TVOUT_PC_ap_return))
				{
					exit(1);
				}
			}

			// ***********************************
			if (i > 0)
			{
				// RTL Name: ap_return
				{
					// bitslice(31, 0)
					// {
						// celement: return(31, 0)
						// {
							sc_lv<32> return_lv0_0_1_0;
						// }
					// }

					// bitslice(31, 0)
					{
						// celement: return(31, 0)
						{
							// carray: (0) => (1) @ (0)
							{
								if (&(AESL_return) != NULL) // check the null address if the c port is array or others
								{
									return_lv0_0_1_0.range(31, 0) = sc_bv<32>(ap_return_pc_buffer.range(31, 0));
								}
							}
						}
					}

					// bitslice(31, 0)
					{
						// celement: return(31, 0)
						{
							// carray: (0) => (1) @ (0)
							{
								// sub                    : 
								// ori_name               : AESL_return
								// sub_1st_elem           : 
								// ori_name_1st_elem      : AESL_return
								// output_left_conversion : AESL_return
								// output_type_conversion : (return_lv0_0_1_0).to_uint64()
								if (&(AESL_return) != NULL) // check the null address if the c port is array or others
								{
									AESL_return = (return_lv0_0_1_0).to_uint64();
								}
							}
						}
					}
				}
			}
		}

		AESL_transaction_pc++;

		return AESL_return;
	}
	else
	{
		CodeState = ENTER_WRAPC;
		static unsigned AESL_transaction;

		static AESL_FILE_HANDLER aesl_fh;

		// "gmem"
		char* tvin_gmem = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_gmem);
		char* tvout_gmem = new char[50];
		aesl_fh.touch(AUTOTB_TVOUT_gmem);

		// "video_in_V_data_V"
		char* tvin_video_in_V_data_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_video_in_V_data_V);
		char* wrapc_stream_size_in_video_in_V_data_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_SIZE_IN_video_in_V_data_V);
		char* wrapc_stream_ingress_status_video_in_V_data_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_INGRESS_STATUS_video_in_V_data_V);

		// "video_in_V_keep_V"
		char* tvin_video_in_V_keep_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_video_in_V_keep_V);
		char* wrapc_stream_size_in_video_in_V_keep_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_SIZE_IN_video_in_V_keep_V);
		char* wrapc_stream_ingress_status_video_in_V_keep_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_INGRESS_STATUS_video_in_V_keep_V);

		// "video_in_V_strb_V"
		char* tvin_video_in_V_strb_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_video_in_V_strb_V);
		char* wrapc_stream_size_in_video_in_V_strb_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_SIZE_IN_video_in_V_strb_V);
		char* wrapc_stream_ingress_status_video_in_V_strb_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_INGRESS_STATUS_video_in_V_strb_V);

		// "video_in_V_user_V"
		char* tvin_video_in_V_user_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_video_in_V_user_V);
		char* wrapc_stream_size_in_video_in_V_user_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_SIZE_IN_video_in_V_user_V);
		char* wrapc_stream_ingress_status_video_in_V_user_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_INGRESS_STATUS_video_in_V_user_V);

		// "video_in_V_last_V"
		char* tvin_video_in_V_last_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_video_in_V_last_V);
		char* wrapc_stream_size_in_video_in_V_last_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_SIZE_IN_video_in_V_last_V);
		char* wrapc_stream_ingress_status_video_in_V_last_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_INGRESS_STATUS_video_in_V_last_V);

		// "video_in_V_id_V"
		char* tvin_video_in_V_id_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_video_in_V_id_V);
		char* wrapc_stream_size_in_video_in_V_id_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_SIZE_IN_video_in_V_id_V);
		char* wrapc_stream_ingress_status_video_in_V_id_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_INGRESS_STATUS_video_in_V_id_V);

		// "video_in_V_dest_V"
		char* tvin_video_in_V_dest_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_video_in_V_dest_V);
		char* wrapc_stream_size_in_video_in_V_dest_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_SIZE_IN_video_in_V_dest_V);
		char* wrapc_stream_ingress_status_video_in_V_dest_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_INGRESS_STATUS_video_in_V_dest_V);

		// "fb_out"
		char* tvin_fb_out = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_fb_out);

		// "stride"
		char* tvin_stride = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_stride);

		// "ap_return"
		char* tvout_ap_return = new char[50];
		aesl_fh.touch(AUTOTB_TVOUT_ap_return);

		CodeState = DUMP_INPUTS;
		static INTER_TCL_FILE tcl_file(INTER_TCL);
		int leading_zero;

		// dump stream tvin: "video_in"
		std::vector<ap_axiu<24, 1, 1, 1 > > aesl_tmp_0;
		int aesl_tmp_1 = 0;
		while (!video_in.empty())
		{
			aesl_tmp_0.push_back(video_in.read());
			aesl_tmp_1++;
		}

		// [[transaction]]
		sprintf(tvin_gmem, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_gmem, tvin_gmem);

		sc_bv<64>* gmem_tvin_wrapc_buffer = new sc_bv<64>[57600];

		// RTL Name: gmem
		{
			// bitslice(63, 0)
			{
				int hls_map_index = 0;
				// celement: fb_out(63, 0)
				{
					// carray: (0) => (57599) @ (1)
					for (int i_0 = 0; i_0 <= 57599; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : fb_out[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : fb_out[0]
						// regulate_c_name       : fb_out
						// input_type_conversion : fb_out[i_0]
						if (&(fb_out[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<64> fb_out_tmp_mem;
							fb_out_tmp_mem = fb_out[i_0];
							gmem_tvin_wrapc_buffer[hls_map_index].range(63, 0) = fb_out_tmp_mem.range(63, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 57600; i++)
		{
			sprintf(tvin_gmem, "%s\n", (gmem_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_gmem, tvin_gmem);
		}

		tcl_file.set_num(57600, &tcl_file.gmem_depth);
		sprintf(tvin_gmem, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_gmem, tvin_gmem);

		// release memory allocation
		delete [] gmem_tvin_wrapc_buffer;

		// [[transaction]]
		sprintf(tvin_fb_out, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_fb_out, tvin_fb_out);

		sc_bv<32> fb_out_tvin_wrapc_buffer;

		// RTL Name: fb_out
		{
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_fb_out, "%s\n", (fb_out_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_fb_out, tvin_fb_out);
		}

		tcl_file.set_num(1, &tcl_file.fb_out_depth);
		sprintf(tvin_fb_out, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_fb_out, tvin_fb_out);

		// [[transaction]]
		sprintf(tvin_stride, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_stride, tvin_stride);

		sc_bv<32> stride_tvin_wrapc_buffer;

		// RTL Name: stride
		{
			// bitslice(31, 0)
			{
				// celement: stride(31, 0)
				{
					// carray: (0) => (0) @ (0)
					{
						// sub                   : 
						// ori_name              : stride
						// sub_1st_elem          : 
						// ori_name_1st_elem     : stride
						// regulate_c_name       : stride
						// input_type_conversion : stride
						if (&(stride) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> stride_tmp_mem;
							stride_tmp_mem = stride;
							stride_tvin_wrapc_buffer.range(31, 0) = stride_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvin_stride, "%s\n", (stride_tvin_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_stride, tvin_stride);
		}

		tcl_file.set_num(1, &tcl_file.stride_depth);
		sprintf(tvin_stride, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_stride, tvin_stride);

		// push back input stream: "video_in"
		for (int i = 0; i < aesl_tmp_1; i++)
		{
			video_in.write(aesl_tmp_0[i]);
		}

// [call_c_dut] ---------->

		CodeState = CALL_C_DUT;
		int AESL_return = RGB2yfb(video_in, fb_out, hsize, vsize, stride, fsize);

		CodeState = DUMP_OUTPUTS;
		// record input size to tv3: "video_in"
		int aesl_tmp_2 = video_in.size();

		// [[transaction]]
		sprintf(tvout_gmem, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVOUT_gmem, tvout_gmem);

		sc_bv<64>* gmem_tvout_wrapc_buffer = new sc_bv<64>[57600];

		// RTL Name: gmem
		{
			// bitslice(63, 0)
			{
				int hls_map_index = 0;
				// celement: fb_out(63, 0)
				{
					// carray: (0) => (57599) @ (1)
					for (int i_0 = 0; i_0 <= 57599; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : fb_out[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : fb_out[0]
						// regulate_c_name       : fb_out
						// input_type_conversion : fb_out[i_0]
						if (&(fb_out[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<64> fb_out_tmp_mem;
							fb_out_tmp_mem = fb_out[i_0];
							gmem_tvout_wrapc_buffer[hls_map_index].range(63, 0) = fb_out_tmp_mem.range(63, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 57600; i++)
		{
			sprintf(tvout_gmem, "%s\n", (gmem_tvout_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVOUT_gmem, tvout_gmem);
		}

		tcl_file.set_num(57600, &tcl_file.gmem_depth);
		sprintf(tvout_gmem, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVOUT_gmem, tvout_gmem);

		// release memory allocation
		delete [] gmem_tvout_wrapc_buffer;

		// [[transaction]]
		sprintf(tvin_video_in_V_data_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_video_in_V_data_V, tvin_video_in_V_data_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_data_V, tvin_video_in_V_data_V);

		sc_bv<24>* video_in_V_data_V_tvin_wrapc_buffer = new sc_bv<24>[aesl_tmp_1 - aesl_tmp_2];

		// RTL Name: video_in_V_data_V
		{
			// bitslice(23, 0)
			{
				int hls_map_index = 0;
				// celement: video_in.V.data.V(23, 0)
				{
					// carray: (0) => (aesl_tmp_1 - aesl_tmp_2 - 1) @ (1)
					for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1; i_0 += 1)
					{
						// carray: (0) => (0) @ (1)
						for (int i_1 = 0; i_1 <= 0; i_1 += 1)
						{
							// sub                   : i_0 i_1
							// ori_name              : aesl_tmp_0[i_0].data
							// sub_1st_elem          : 0 0
							// ori_name_1st_elem     : aesl_tmp_0[0].data
							// regulate_c_name       : video_in_V_data_V
							// input_type_conversion : (aesl_tmp_0[i_0].data).to_string(2).c_str()
							if (&(aesl_tmp_0[0].data) != NULL) // check the null address if the c port is array or others
							{
								sc_lv<24> video_in_V_data_V_tmp_mem;
								video_in_V_data_V_tmp_mem = (aesl_tmp_0[i_0].data).to_string(2).c_str();
								video_in_V_data_V_tvin_wrapc_buffer[hls_map_index].range(23, 0) = video_in_V_data_V_tmp_mem.range(23, 0);
                                 		       hls_map_index++;
							}
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			sprintf(tvin_video_in_V_data_V, "%s\n", (video_in_V_data_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_video_in_V_data_V, tvin_video_in_V_data_V);
		}

		// dump stream ingress status to file
     if (aesl_tmp_1 > aesl_tmp_2)
     {
		sc_int<32> stream_ingress_size_video_in_V_data_V = aesl_tmp_1;
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_data_V, stream_ingress_size_video_in_V_data_V.to_string().c_str());
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_data_V, "\n");

		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			stream_ingress_size_video_in_V_data_V--;
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_data_V, stream_ingress_size_video_in_V_data_V.to_string().c_str());
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_data_V, "\n");
		}
     }
     else {
		    sc_int<32> stream_ingress_size_video_in_V_data_V = 0;
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_data_V, stream_ingress_size_video_in_V_data_V.to_string().c_str());
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_data_V, "\n");
     }

		tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2, &tcl_file.video_in_V_data_V_depth);
		sprintf(tvin_video_in_V_data_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_video_in_V_data_V, tvin_video_in_V_data_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_data_V, tvin_video_in_V_data_V);

		// release memory allocation
		delete [] video_in_V_data_V_tvin_wrapc_buffer;

		// dump stream size
		sprintf(wrapc_stream_size_in_video_in_V_data_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, wrapc_stream_size_in_video_in_V_data_V);
		sprintf(wrapc_stream_size_in_video_in_V_data_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, wrapc_stream_size_in_video_in_V_data_V);
		sprintf(wrapc_stream_size_in_video_in_V_data_V, "[[/transaction]] \n");
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_data_V, wrapc_stream_size_in_video_in_V_data_V);

		// [[transaction]]
		sprintf(tvin_video_in_V_keep_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_video_in_V_keep_V, tvin_video_in_V_keep_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_keep_V, tvin_video_in_V_keep_V);

		sc_bv<3>* video_in_V_keep_V_tvin_wrapc_buffer = new sc_bv<3>[aesl_tmp_1 - aesl_tmp_2];

		// RTL Name: video_in_V_keep_V
		{
			// bitslice(2, 0)
			{
				int hls_map_index = 0;
				// celement: video_in.V.keep.V(2, 0)
				{
					// carray: (0) => (aesl_tmp_1 - aesl_tmp_2 - 1) @ (1)
					for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1; i_0 += 1)
					{
						// carray: (0) => (0) @ (1)
						for (int i_1 = 0; i_1 <= 0; i_1 += 1)
						{
							// sub                   : i_0 i_1
							// ori_name              : aesl_tmp_0[i_0].keep
							// sub_1st_elem          : 0 0
							// ori_name_1st_elem     : aesl_tmp_0[0].keep
							// regulate_c_name       : video_in_V_keep_V
							// input_type_conversion : (aesl_tmp_0[i_0].keep).to_string(2).c_str()
							if (&(aesl_tmp_0[0].keep) != NULL) // check the null address if the c port is array or others
							{
								sc_lv<3> video_in_V_keep_V_tmp_mem;
								video_in_V_keep_V_tmp_mem = (aesl_tmp_0[i_0].keep).to_string(2).c_str();
								video_in_V_keep_V_tvin_wrapc_buffer[hls_map_index].range(2, 0) = video_in_V_keep_V_tmp_mem.range(2, 0);
                                 		       hls_map_index++;
							}
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			sprintf(tvin_video_in_V_keep_V, "%s\n", (video_in_V_keep_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_video_in_V_keep_V, tvin_video_in_V_keep_V);
		}

		// dump stream ingress status to file
     if (aesl_tmp_1 > aesl_tmp_2)
     {
		sc_int<32> stream_ingress_size_video_in_V_keep_V = aesl_tmp_1;
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_keep_V, stream_ingress_size_video_in_V_keep_V.to_string().c_str());
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_keep_V, "\n");

		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			stream_ingress_size_video_in_V_keep_V--;
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_keep_V, stream_ingress_size_video_in_V_keep_V.to_string().c_str());
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_keep_V, "\n");
		}
     }
     else {
		    sc_int<32> stream_ingress_size_video_in_V_keep_V = 0;
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_keep_V, stream_ingress_size_video_in_V_keep_V.to_string().c_str());
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_keep_V, "\n");
     }

		tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2, &tcl_file.video_in_V_keep_V_depth);
		sprintf(tvin_video_in_V_keep_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_video_in_V_keep_V, tvin_video_in_V_keep_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_keep_V, tvin_video_in_V_keep_V);

		// release memory allocation
		delete [] video_in_V_keep_V_tvin_wrapc_buffer;

		// dump stream size
		sprintf(wrapc_stream_size_in_video_in_V_keep_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_keep_V, wrapc_stream_size_in_video_in_V_keep_V);
		sprintf(wrapc_stream_size_in_video_in_V_keep_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_keep_V, wrapc_stream_size_in_video_in_V_keep_V);
		sprintf(wrapc_stream_size_in_video_in_V_keep_V, "[[/transaction]] \n");
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_keep_V, wrapc_stream_size_in_video_in_V_keep_V);

		// [[transaction]]
		sprintf(tvin_video_in_V_strb_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_video_in_V_strb_V, tvin_video_in_V_strb_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_strb_V, tvin_video_in_V_strb_V);

		sc_bv<3>* video_in_V_strb_V_tvin_wrapc_buffer = new sc_bv<3>[aesl_tmp_1 - aesl_tmp_2];

		// RTL Name: video_in_V_strb_V
		{
			// bitslice(2, 0)
			{
				int hls_map_index = 0;
				// celement: video_in.V.strb.V(2, 0)
				{
					// carray: (0) => (aesl_tmp_1 - aesl_tmp_2 - 1) @ (1)
					for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1; i_0 += 1)
					{
						// carray: (0) => (0) @ (1)
						for (int i_1 = 0; i_1 <= 0; i_1 += 1)
						{
							// sub                   : i_0 i_1
							// ori_name              : aesl_tmp_0[i_0].strb
							// sub_1st_elem          : 0 0
							// ori_name_1st_elem     : aesl_tmp_0[0].strb
							// regulate_c_name       : video_in_V_strb_V
							// input_type_conversion : (aesl_tmp_0[i_0].strb).to_string(2).c_str()
							if (&(aesl_tmp_0[0].strb) != NULL) // check the null address if the c port is array or others
							{
								sc_lv<3> video_in_V_strb_V_tmp_mem;
								video_in_V_strb_V_tmp_mem = (aesl_tmp_0[i_0].strb).to_string(2).c_str();
								video_in_V_strb_V_tvin_wrapc_buffer[hls_map_index].range(2, 0) = video_in_V_strb_V_tmp_mem.range(2, 0);
                                 		       hls_map_index++;
							}
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			sprintf(tvin_video_in_V_strb_V, "%s\n", (video_in_V_strb_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_video_in_V_strb_V, tvin_video_in_V_strb_V);
		}

		// dump stream ingress status to file
     if (aesl_tmp_1 > aesl_tmp_2)
     {
		sc_int<32> stream_ingress_size_video_in_V_strb_V = aesl_tmp_1;
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_strb_V, stream_ingress_size_video_in_V_strb_V.to_string().c_str());
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_strb_V, "\n");

		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			stream_ingress_size_video_in_V_strb_V--;
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_strb_V, stream_ingress_size_video_in_V_strb_V.to_string().c_str());
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_strb_V, "\n");
		}
     }
     else {
		    sc_int<32> stream_ingress_size_video_in_V_strb_V = 0;
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_strb_V, stream_ingress_size_video_in_V_strb_V.to_string().c_str());
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_strb_V, "\n");
     }

		tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2, &tcl_file.video_in_V_strb_V_depth);
		sprintf(tvin_video_in_V_strb_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_video_in_V_strb_V, tvin_video_in_V_strb_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_strb_V, tvin_video_in_V_strb_V);

		// release memory allocation
		delete [] video_in_V_strb_V_tvin_wrapc_buffer;

		// dump stream size
		sprintf(wrapc_stream_size_in_video_in_V_strb_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_strb_V, wrapc_stream_size_in_video_in_V_strb_V);
		sprintf(wrapc_stream_size_in_video_in_V_strb_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_strb_V, wrapc_stream_size_in_video_in_V_strb_V);
		sprintf(wrapc_stream_size_in_video_in_V_strb_V, "[[/transaction]] \n");
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_strb_V, wrapc_stream_size_in_video_in_V_strb_V);

		// [[transaction]]
		sprintf(tvin_video_in_V_user_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_video_in_V_user_V, tvin_video_in_V_user_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_user_V, tvin_video_in_V_user_V);

		sc_bv<1>* video_in_V_user_V_tvin_wrapc_buffer = new sc_bv<1>[aesl_tmp_1 - aesl_tmp_2];

		// RTL Name: video_in_V_user_V
		{
			// bitslice(0, 0)
			{
				int hls_map_index = 0;
				// celement: video_in.V.user.V(0, 0)
				{
					// carray: (0) => (aesl_tmp_1 - aesl_tmp_2 - 1) @ (1)
					for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1; i_0 += 1)
					{
						// carray: (0) => (0) @ (1)
						for (int i_1 = 0; i_1 <= 0; i_1 += 1)
						{
							// sub                   : i_0 i_1
							// ori_name              : aesl_tmp_0[i_0].user
							// sub_1st_elem          : 0 0
							// ori_name_1st_elem     : aesl_tmp_0[0].user
							// regulate_c_name       : video_in_V_user_V
							// input_type_conversion : (aesl_tmp_0[i_0].user).to_string(2).c_str()
							if (&(aesl_tmp_0[0].user) != NULL) // check the null address if the c port is array or others
							{
								sc_lv<1> video_in_V_user_V_tmp_mem;
								video_in_V_user_V_tmp_mem = (aesl_tmp_0[i_0].user).to_string(2).c_str();
								video_in_V_user_V_tvin_wrapc_buffer[hls_map_index].range(0, 0) = video_in_V_user_V_tmp_mem.range(0, 0);
                                 		       hls_map_index++;
							}
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			sprintf(tvin_video_in_V_user_V, "%s\n", (video_in_V_user_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_video_in_V_user_V, tvin_video_in_V_user_V);
		}

		// dump stream ingress status to file
     if (aesl_tmp_1 > aesl_tmp_2)
     {
		sc_int<32> stream_ingress_size_video_in_V_user_V = aesl_tmp_1;
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_user_V, stream_ingress_size_video_in_V_user_V.to_string().c_str());
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_user_V, "\n");

		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			stream_ingress_size_video_in_V_user_V--;
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_user_V, stream_ingress_size_video_in_V_user_V.to_string().c_str());
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_user_V, "\n");
		}
     }
     else {
		    sc_int<32> stream_ingress_size_video_in_V_user_V = 0;
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_user_V, stream_ingress_size_video_in_V_user_V.to_string().c_str());
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_user_V, "\n");
     }

		tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2, &tcl_file.video_in_V_user_V_depth);
		sprintf(tvin_video_in_V_user_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_video_in_V_user_V, tvin_video_in_V_user_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_user_V, tvin_video_in_V_user_V);

		// release memory allocation
		delete [] video_in_V_user_V_tvin_wrapc_buffer;

		// dump stream size
		sprintf(wrapc_stream_size_in_video_in_V_user_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_user_V, wrapc_stream_size_in_video_in_V_user_V);
		sprintf(wrapc_stream_size_in_video_in_V_user_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_user_V, wrapc_stream_size_in_video_in_V_user_V);
		sprintf(wrapc_stream_size_in_video_in_V_user_V, "[[/transaction]] \n");
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_user_V, wrapc_stream_size_in_video_in_V_user_V);

		// [[transaction]]
		sprintf(tvin_video_in_V_last_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_video_in_V_last_V, tvin_video_in_V_last_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_last_V, tvin_video_in_V_last_V);

		sc_bv<1>* video_in_V_last_V_tvin_wrapc_buffer = new sc_bv<1>[aesl_tmp_1 - aesl_tmp_2];

		// RTL Name: video_in_V_last_V
		{
			// bitslice(0, 0)
			{
				int hls_map_index = 0;
				// celement: video_in.V.last.V(0, 0)
				{
					// carray: (0) => (aesl_tmp_1 - aesl_tmp_2 - 1) @ (1)
					for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1; i_0 += 1)
					{
						// carray: (0) => (0) @ (1)
						for (int i_1 = 0; i_1 <= 0; i_1 += 1)
						{
							// sub                   : i_0 i_1
							// ori_name              : aesl_tmp_0[i_0].last
							// sub_1st_elem          : 0 0
							// ori_name_1st_elem     : aesl_tmp_0[0].last
							// regulate_c_name       : video_in_V_last_V
							// input_type_conversion : (aesl_tmp_0[i_0].last).to_string(2).c_str()
							if (&(aesl_tmp_0[0].last) != NULL) // check the null address if the c port is array or others
							{
								sc_lv<1> video_in_V_last_V_tmp_mem;
								video_in_V_last_V_tmp_mem = (aesl_tmp_0[i_0].last).to_string(2).c_str();
								video_in_V_last_V_tvin_wrapc_buffer[hls_map_index].range(0, 0) = video_in_V_last_V_tmp_mem.range(0, 0);
                                 		       hls_map_index++;
							}
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			sprintf(tvin_video_in_V_last_V, "%s\n", (video_in_V_last_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_video_in_V_last_V, tvin_video_in_V_last_V);
		}

		// dump stream ingress status to file
     if (aesl_tmp_1 > aesl_tmp_2)
     {
		sc_int<32> stream_ingress_size_video_in_V_last_V = aesl_tmp_1;
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_last_V, stream_ingress_size_video_in_V_last_V.to_string().c_str());
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_last_V, "\n");

		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			stream_ingress_size_video_in_V_last_V--;
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_last_V, stream_ingress_size_video_in_V_last_V.to_string().c_str());
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_last_V, "\n");
		}
     }
     else {
		    sc_int<32> stream_ingress_size_video_in_V_last_V = 0;
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_last_V, stream_ingress_size_video_in_V_last_V.to_string().c_str());
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_last_V, "\n");
     }

		tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2, &tcl_file.video_in_V_last_V_depth);
		sprintf(tvin_video_in_V_last_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_video_in_V_last_V, tvin_video_in_V_last_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_last_V, tvin_video_in_V_last_V);

		// release memory allocation
		delete [] video_in_V_last_V_tvin_wrapc_buffer;

		// dump stream size
		sprintf(wrapc_stream_size_in_video_in_V_last_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_last_V, wrapc_stream_size_in_video_in_V_last_V);
		sprintf(wrapc_stream_size_in_video_in_V_last_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_last_V, wrapc_stream_size_in_video_in_V_last_V);
		sprintf(wrapc_stream_size_in_video_in_V_last_V, "[[/transaction]] \n");
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_last_V, wrapc_stream_size_in_video_in_V_last_V);

		// [[transaction]]
		sprintf(tvin_video_in_V_id_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_video_in_V_id_V, tvin_video_in_V_id_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_id_V, tvin_video_in_V_id_V);

		sc_bv<1>* video_in_V_id_V_tvin_wrapc_buffer = new sc_bv<1>[aesl_tmp_1 - aesl_tmp_2];

		// RTL Name: video_in_V_id_V
		{
			// bitslice(0, 0)
			{
				int hls_map_index = 0;
				// celement: video_in.V.id.V(0, 0)
				{
					// carray: (0) => (aesl_tmp_1 - aesl_tmp_2 - 1) @ (1)
					for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1; i_0 += 1)
					{
						// carray: (0) => (0) @ (1)
						for (int i_1 = 0; i_1 <= 0; i_1 += 1)
						{
							// sub                   : i_0 i_1
							// ori_name              : aesl_tmp_0[i_0].id
							// sub_1st_elem          : 0 0
							// ori_name_1st_elem     : aesl_tmp_0[0].id
							// regulate_c_name       : video_in_V_id_V
							// input_type_conversion : (aesl_tmp_0[i_0].id).to_string(2).c_str()
							if (&(aesl_tmp_0[0].id) != NULL) // check the null address if the c port is array or others
							{
								sc_lv<1> video_in_V_id_V_tmp_mem;
								video_in_V_id_V_tmp_mem = (aesl_tmp_0[i_0].id).to_string(2).c_str();
								video_in_V_id_V_tvin_wrapc_buffer[hls_map_index].range(0, 0) = video_in_V_id_V_tmp_mem.range(0, 0);
                                 		       hls_map_index++;
							}
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			sprintf(tvin_video_in_V_id_V, "%s\n", (video_in_V_id_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_video_in_V_id_V, tvin_video_in_V_id_V);
		}

		// dump stream ingress status to file
     if (aesl_tmp_1 > aesl_tmp_2)
     {
		sc_int<32> stream_ingress_size_video_in_V_id_V = aesl_tmp_1;
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_id_V, stream_ingress_size_video_in_V_id_V.to_string().c_str());
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_id_V, "\n");

		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			stream_ingress_size_video_in_V_id_V--;
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_id_V, stream_ingress_size_video_in_V_id_V.to_string().c_str());
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_id_V, "\n");
		}
     }
     else {
		    sc_int<32> stream_ingress_size_video_in_V_id_V = 0;
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_id_V, stream_ingress_size_video_in_V_id_V.to_string().c_str());
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_id_V, "\n");
     }

		tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2, &tcl_file.video_in_V_id_V_depth);
		sprintf(tvin_video_in_V_id_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_video_in_V_id_V, tvin_video_in_V_id_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_id_V, tvin_video_in_V_id_V);

		// release memory allocation
		delete [] video_in_V_id_V_tvin_wrapc_buffer;

		// dump stream size
		sprintf(wrapc_stream_size_in_video_in_V_id_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_id_V, wrapc_stream_size_in_video_in_V_id_V);
		sprintf(wrapc_stream_size_in_video_in_V_id_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_id_V, wrapc_stream_size_in_video_in_V_id_V);
		sprintf(wrapc_stream_size_in_video_in_V_id_V, "[[/transaction]] \n");
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_id_V, wrapc_stream_size_in_video_in_V_id_V);

		// [[transaction]]
		sprintf(tvin_video_in_V_dest_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_video_in_V_dest_V, tvin_video_in_V_dest_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_dest_V, tvin_video_in_V_dest_V);

		sc_bv<1>* video_in_V_dest_V_tvin_wrapc_buffer = new sc_bv<1>[aesl_tmp_1 - aesl_tmp_2];

		// RTL Name: video_in_V_dest_V
		{
			// bitslice(0, 0)
			{
				int hls_map_index = 0;
				// celement: video_in.V.dest.V(0, 0)
				{
					// carray: (0) => (aesl_tmp_1 - aesl_tmp_2 - 1) @ (1)
					for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1; i_0 += 1)
					{
						// carray: (0) => (0) @ (1)
						for (int i_1 = 0; i_1 <= 0; i_1 += 1)
						{
							// sub                   : i_0 i_1
							// ori_name              : aesl_tmp_0[i_0].dest
							// sub_1st_elem          : 0 0
							// ori_name_1st_elem     : aesl_tmp_0[0].dest
							// regulate_c_name       : video_in_V_dest_V
							// input_type_conversion : (aesl_tmp_0[i_0].dest).to_string(2).c_str()
							if (&(aesl_tmp_0[0].dest) != NULL) // check the null address if the c port is array or others
							{
								sc_lv<1> video_in_V_dest_V_tmp_mem;
								video_in_V_dest_V_tmp_mem = (aesl_tmp_0[i_0].dest).to_string(2).c_str();
								video_in_V_dest_V_tvin_wrapc_buffer[hls_map_index].range(0, 0) = video_in_V_dest_V_tmp_mem.range(0, 0);
                                 		       hls_map_index++;
							}
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			sprintf(tvin_video_in_V_dest_V, "%s\n", (video_in_V_dest_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_video_in_V_dest_V, tvin_video_in_V_dest_V);
		}

		// dump stream ingress status to file
     if (aesl_tmp_1 > aesl_tmp_2)
     {
		sc_int<32> stream_ingress_size_video_in_V_dest_V = aesl_tmp_1;
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_dest_V, stream_ingress_size_video_in_V_dest_V.to_string().c_str());
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_dest_V, "\n");

		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			stream_ingress_size_video_in_V_dest_V--;
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_dest_V, stream_ingress_size_video_in_V_dest_V.to_string().c_str());
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_dest_V, "\n");
		}
     }
     else {
		    sc_int<32> stream_ingress_size_video_in_V_dest_V = 0;
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_dest_V, stream_ingress_size_video_in_V_dest_V.to_string().c_str());
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_dest_V, "\n");
     }

		tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2, &tcl_file.video_in_V_dest_V_depth);
		sprintf(tvin_video_in_V_dest_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_video_in_V_dest_V, tvin_video_in_V_dest_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_video_in_V_dest_V, tvin_video_in_V_dest_V);

		// release memory allocation
		delete [] video_in_V_dest_V_tvin_wrapc_buffer;

		// dump stream size
		sprintf(wrapc_stream_size_in_video_in_V_dest_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_dest_V, wrapc_stream_size_in_video_in_V_dest_V);
		sprintf(wrapc_stream_size_in_video_in_V_dest_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_dest_V, wrapc_stream_size_in_video_in_V_dest_V);
		sprintf(wrapc_stream_size_in_video_in_V_dest_V, "[[/transaction]] \n");
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_video_in_V_dest_V, wrapc_stream_size_in_video_in_V_dest_V);

		// [[transaction]]
		sprintf(tvout_ap_return, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVOUT_ap_return, tvout_ap_return);

		sc_bv<32> ap_return_tvout_wrapc_buffer;

		// RTL Name: ap_return
		{
			// bitslice(31, 0)
			{
				// celement: return(31, 0)
				{
					// carray: (0) => (1) @ (0)
					{
						// sub                   : 
						// ori_name              : AESL_return
						// sub_1st_elem          : 
						// ori_name_1st_elem     : AESL_return
						// regulate_c_name       : return
						// input_type_conversion : AESL_return
						if (&(AESL_return) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> return_tmp_mem;
							return_tmp_mem = AESL_return;
							ap_return_tvout_wrapc_buffer.range(31, 0) = return_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 1; i++)
		{
			sprintf(tvout_ap_return, "%s\n", (ap_return_tvout_wrapc_buffer).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVOUT_ap_return, tvout_ap_return);
		}

		tcl_file.set_num(1, &tcl_file.ap_return_depth);
		sprintf(tvout_ap_return, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVOUT_ap_return, tvout_ap_return);

		CodeState = DELETE_CHAR_BUFFERS;
		// release memory allocation: "gmem"
		delete [] tvout_gmem;
		delete [] tvin_gmem;
		// release memory allocation: "video_in_V_data_V"
		delete [] tvin_video_in_V_data_V;
		delete [] wrapc_stream_size_in_video_in_V_data_V;
		// release memory allocation: "video_in_V_keep_V"
		delete [] tvin_video_in_V_keep_V;
		delete [] wrapc_stream_size_in_video_in_V_keep_V;
		// release memory allocation: "video_in_V_strb_V"
		delete [] tvin_video_in_V_strb_V;
		delete [] wrapc_stream_size_in_video_in_V_strb_V;
		// release memory allocation: "video_in_V_user_V"
		delete [] tvin_video_in_V_user_V;
		delete [] wrapc_stream_size_in_video_in_V_user_V;
		// release memory allocation: "video_in_V_last_V"
		delete [] tvin_video_in_V_last_V;
		delete [] wrapc_stream_size_in_video_in_V_last_V;
		// release memory allocation: "video_in_V_id_V"
		delete [] tvin_video_in_V_id_V;
		delete [] wrapc_stream_size_in_video_in_V_id_V;
		// release memory allocation: "video_in_V_dest_V"
		delete [] tvin_video_in_V_dest_V;
		delete [] wrapc_stream_size_in_video_in_V_dest_V;
		// release memory allocation: "fb_out"
		delete [] tvin_fb_out;
		// release memory allocation: "stride"
		delete [] tvin_stride;
		// release memory allocation: "ap_return"
		delete [] tvout_ap_return;

		AESL_transaction++;

		tcl_file.set_num(AESL_transaction , &tcl_file.trans_num);

		return AESL_return;
	}
}

