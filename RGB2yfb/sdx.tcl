# ==============================================================
# File generated on Tue Feb 12 02:58:43 JST 2019
# Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
# SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
# IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
# Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
# ==============================================================
add_files -tb ../RGB2yfb/src/RGB2yfb.h -cflags { -Wno-unknown-pragmas}
add_files -tb ../RGB2yfb/src/testbench/RGB2yfb_tb.cpp -cflags { -Wno-unknown-pragmas}
add_files -tb ../RGB2yfb/src/testbench/dataset -cflags { -Wno-unknown-pragmas}
add_files RGB2FB/RGB2yfb/src/RGB2yfb.cpp
add_files RGB2FB/RGB2yfb/src/RGB2yfb.h
set_part xc7z010clg400-1
create_clock -name default -period 7.5
set_clock_uncertainty 12.5% default
config_compile -no_signed_zeros=0
config_compile -unsafe_math_optimizations=0
config_export -format=ip_catalog
config_export -rtl=verilog
config_export -vivado_phys_opt=place
config_export -vivado_report_level=0
config_sdx -optimization_level=none
config_sdx -target=none
config_schedule -effort=medium
config_schedule -relax_ii_for_timing=0
config_bind -effort=medium
