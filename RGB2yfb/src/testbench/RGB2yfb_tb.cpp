#include "../RGB2yfb.h"

#include <stdio.h>
#include <string.h>
#include <ap_int.h>
#include <hls_stream.h>
#include <ap_axi_sdata.h>
#include <hls_video.h>
#include <hls_opencv.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#ifdef SMALL
	#define HSIZE 320
	#define VSIZE 180
	#define INPUT_IMAGE "./dataset/test_small.jpg"
#else
	#define HSIZE 1280
	#define VSIZE 720
	#define INPUT_IMAGE "./dataset/test.jpg"
#endif
#define OUTPUT_IMAGE "./test_result.bmp"

int main(int argc, char **argv){

	cv::Mat src = cv::imread(INPUT_IMAGE);

	AXIS_RGB_STREAM src_axi;

	cvMat2AXIvideo(src, src_axi);

	uint64_t fb[HSIZE * VSIZE];


	RGB2yfb(src_axi, fb, HSIZE, VSIZE, HSIZE, HSIZE*VSIZE);
	cv::Mat matfb(VSIZE, HSIZE, CV_8UC1, (void *)fb);
	cv::imwrite(OUTPUT_IMAGE, matfb);

	return EXIT_SUCCESS;

}
