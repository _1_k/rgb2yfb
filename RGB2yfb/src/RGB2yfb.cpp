#include <ap_int.h>
#include <stdint.h>
#include <hls_stream.h>
#include <ap_axi_sdata.h>
#include <hls_video.h>

#include "RGB2yfb.h"
// #define DEBUG

int RGB2yfb(AXIS_RGB_STREAM &video_in,
		uint64_t *fb_out,
		uint32_t hsize, uint32_t vsize, uint32_t stride, uint32_t fsize){
#ifdef DEBUG
	int axi_read_cnt = 0;
#endif

	#pragma HLS INTERFACE s_axilite port=return
	#pragma HLS INTERFACE axis port=video_in
	#pragma HLS INTERFACE m_axi port=fb_out offset=slave depth=57600

	#pragma HLS INTERFACE s_axilite port=hsize
	#pragma HLS INTERFACE s_axilite port=vsize
	#pragma HLS INTERFACE s_axilite port=fsize
	#pragma HLS INTERFACE s_axilite port=stride
	for(int i = 0; i < 2; i++){
		AXIS_RGB_PIXEL pix;
		bool is_first_line = 1;
	frame_detection:
		do {
			while(video_in.empty())
				;
			video_in >> pix;
	#ifdef DEBUG
			printf("read:%d\n", axi_read_cnt++);
	#endif
		} while (pix.user == 0);

	frame_loop:
		for(uint32_t line_idx = 0; line_idx < FB_VSIZE; line_idx++){

		read_line_to_linebuf:
			for(uint32_t blk_idx = 0; blk_idx < (FB_HSIZE / 8); blk_idx++){
			#pragma HLS PIPELINE II=8
				uint64_t blk;
			get_blk:
				for(uint8_t blk_pix_idx = 0; blk_pix_idx < 8; blk_pix_idx++){
				#pragma HLS PIPELINE II=1
				#pragma HLS UNROLL factor=1

					if(!is_first_line){
						video_in >> pix;
	#ifdef DEBUG
						printf("read:%d\n", axi_read_cnt++);
	#endif
					}

					is_first_line = 0;
					uint64_t mask = 0xffLL << 56;
					mask &= (((uint64_t)conv_rgb2y(pix.data)) << 56);
					blk = (blk >> 8)|mask;
				}

			write_blk_to_buf:
				*(fb_out + blk_idx) = blk;
	#ifdef DEBUG
				printf("\tFB W offset: 0x%08x (%u)\n", (fb_out + blk_idx), (fb_out + blk_idx));
	#endif /* DEBUG */
			}
			fb_out += (stride / 8);
		}
	}
	return 0;
}

int conv_rgb2y(int rgb){
#pragma HLS INLINE
	int r, g, b, y_f, y;

	b = rgb & 0xff;
	g = (rgb >> 8) & 0xff;
	r = (rgb >> 16) & 0xff;

	/* 0.299R + 0.587G + 0.114B の係数に 256 �? */
	y_f = (77 * r) + (150 * g) + (29 * b);

	/* 256 で割�? */
	y = y_f >> 8;

	return y;
}
