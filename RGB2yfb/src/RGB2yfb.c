//
// lap_filter_rgb.cpp
// 2015/08/21 by marsee
//

#include <stdio.h>
#include <string.h>
#include <ap_int.h>
#include <hls_stream.h>

#include "RGB2yfb.h"
int conv_rgb2y(int rgb);


void lap_filter_rgb(ap_uint <1>enable){
#pragma HLS INTERFACE ap_ctrl_hs port=return
#pragma HLS INTERFACE ap_hs register port=video_out
#pragma HLS INTERFACE ap_hs register port=video_in
#pragma HLS INTERFACE ap_none register port=lap_fil_enable
	return;
}
	// RGBからYへの変換
	// RGBのフォーマットは、{8'd0, R(8bits), G(8bits), B(8bits)}, 1pixel = 32bits
	// 輝度信号Yのみに変換する。変換式は、Y =  0.299R + 0.587G + 0.114B
	// "YUVフォーマット及び YUV<->RGB変換"を参考にした。http://vision.kuee.kyoto-u.ac.jp/~hiroaki/firewire/yuv.html
	//　2013/09/27 : float を止めて、すべてint にした
int conv_rgb2y(int rgb){
	int r, g, b, y_f;
	int y;

	b = rgb & 0xff;
	g = (rgb>>8) & 0xff;
	r = (rgb>>16) & 0xff;

	y_f = 77*r + 150*g + 29*b; //y_f = 0.299*r + 0.587*g + 0.114*b;の係数に256倍した
	y = y_f >> 8; // 256で割る

	return(y);
}
