int conv_rgb2y(int rgb);

// #define SMALL

#ifdef SMALL
	#define FB_HSIZE 320
	#define FB_VSIZE 180
#else
	#define FB_HSIZE 1280
	#define FB_VSIZE 720
#endif
#ifndef __RGB2YFB__
#define __RGB2YFB__
#include <stdio.h>
#include <string.h>
#include <ap_int.h>
#include <hls_stream.h>
#include <ap_axi_sdata.h>
#include <hls_video.h>
#include "ap_int.h"

#define FRAME_BUFFER_MAX_WIDTH 1920
#define BLK_BUFFER_SIZE (FRAME_BUFFER_MAX_WIDTH / 8)

typedef ap_axiu<24,1,1,1> AXIS_RGB_PIXEL;
typedef hls::stream<AXIS_RGB_PIXEL> AXIS_RGB_STREAM;
int RGB2yfb( AXIS_RGB_STREAM &video_in,
		     uint64_t *fb_out,
			 uint32_t hsize, uint32_t vsize, uint32_t stride, uint32_t fsize);

#endif
